# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.2.0 (2020-01-01)

### Bug Fixes

- errCb argument fixable tests and functionality ([f89ebc0](https://gitlab.com/codsen/codsen/commit/f89ebc03858734e67e4ffd4db9a89b4116fa07ea))
- trailing whitespace clause fix ([9ef1181](https://gitlab.com/codsen/codsen/commit/9ef1181c20e15b135b791ccc53343d135241cf22))

### Features

- opts.innerWhitespaceAllowed ([65dfb1f](https://gitlab.com/codsen/codsen/commit/65dfb1fa5fed0b201b3e5001d418b49b5f94917c))

## 1.1.0 (2019-12-27)

### Bug Fixes

- add more tests and cover opts.trailingWhitespaceOK ([335c6d6](https://gitlab.com/codsen/codsen/commit/335c6d63fa3ff0b324f5d191350fc0e593e88430))
- returned ranges is always array of arrays, not just a single range/array ([f9469a1](https://gitlab.com/codsen/codsen/commit/f9469a1f53aa13c7f85eb953e35428a735232cd1))

### Features

- init ([72acd9a](https://gitlab.com/codsen/codsen/commit/72acd9a022fbf4db0116e3d19b0a5dc451581200))

## 1.0.0 (2019-12-26)

- ✨ First public release
