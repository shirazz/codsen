# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.0.0 (2020-01-05)

### Features

- init ([00776db](https://gitlab.com/codsen/codsen/commit/00776db3a81ecd9a683580fd459a756c462338f5))

### BREAKING CHANGES

- init

## 1.0.0 (2020-01-05)

- ✨ First public release
