# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.1.0 (2020-04-04)

### Features

- init ([f806c99](https://gitlab.com/codsen/codsen/commit/f806c9960d7edecc17e353d59ca9965966cf331d))
- lookaheads ([d9acc8b](https://gitlab.com/codsen/codsen/commit/d9acc8b338a8911327148e13e2c8098c809257e5))

## 1.0.0 (2020-03-26)

- ✨ First public release.
