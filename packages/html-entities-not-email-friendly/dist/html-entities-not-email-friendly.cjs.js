/**
 * html-entities-not-email-friendly
 * All HTML entities which are not email template friendly
 * Version: 0.1.14
 * Author: Roy Revelt, Codsen Ltd
 * License: MIT
 * Homepage: https://gitlab.com/codsen/codsen/tree/master/packages/all-named-html-entities
 */

'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var AMP = "amp";
var Abreve = "#x102";
var Acy = "#x410";
var Afr = "#x1D504";
var Amacr = "#x100";
var And = "#x2A53";
var Aogon = "#x104";
var Aopf = "#x1D538";
var ApplyFunction = "#x2061";
var Ascr = "#x1D49C";
var Assign = "#x2254";
var Backslash = "#x2216";
var Barv = "#x2AE7";
var Barwed = "#x2306";
var Bcy = "#x411";
var Because = "#x2235";
var Bernoullis = "#x212C";
var Bfr = "#x1D505";
var Bopf = "#x1D539";
var Breve = "#x2D8";
var Bscr = "#x212C";
var Bumpeq = "#x224E";
var CHcy = "#x427";
var COPY = "copy";
var Cacute = "#x106";
var Cap = "#x22D2";
var CapitalDifferentialD = "#x2145";
var Cayleys = "#x212D";
var Ccaron = "#x10C";
var Ccirc = "#x108";
var Cconint = "#x2230";
var Cdot = "#x10A";
var Cedilla = "cedil";
var CenterDot = "middot";
var Cfr = "#x212D";
var CircleDot = "#x2299";
var CircleMinus = "#x2296";
var CirclePlus = "oplus";
var CircleTimes = "otimes";
var ClockwiseContourIntegral = "#x2232";
var CloseCurlyDoubleQuote = "rdquo";
var CloseCurlyQuote = "rsquo";
var Colon = "#x2237";
var Colone = "#x2A74";
var Congruent = "equiv";
var Conint = "#x222F";
var ContourIntegral = "#x222E";
var Copf = "#x2102";
var Coproduct = "#x2210";
var CounterClockwiseContourIntegral = "#x2233";
var Cross = "#x2A2F";
var Cscr = "#x1D49E";
var Cup = "#x22D3";
var CupCap = "#x224D";
var DD = "#x2145";
var DDotrahd = "#x2911";
var DJcy = "#x402";
var DScy = "#x405";
var DZcy = "#x40F";
var Darr = "#x21A1";
var Dashv = "#x2AE4";
var Dcaron = "#x10E";
var Dcy = "#x414";
var Del = "#x2207";
var Dfr = "#x1D507";
var DiacriticalAcute = "acute";
var DiacriticalDot = "#x2D9";
var DiacriticalDoubleAcute = "#x2DD";
var DiacriticalGrave = "#x60";
var DiacriticalTilde = "tilde";
var Diamond = "#x22C4";
var DifferentialD = "#x2146";
var Dopf = "#x1D53B";
var Dot = "#xA8";
var DotDot = "#x20DC";
var DotEqual = "#x2250";
var DoubleContourIntegral = "#x222F";
var DoubleDot = "#xA8";
var DoubleDownArrow = "dArr";
var DoubleLeftArrow = "lArr";
var DoubleLeftRightArrow = "#x21D4";
var DoubleLeftTee = "#x2AE4";
var DoubleLongLeftArrow = "#x27F8";
var DoubleLongLeftRightArrow = "#x27FA";
var DoubleLongRightArrow = "#x27F9";
var DoubleRightArrow = "rArr";
var DoubleRightTee = "#x22A8";
var DoubleUpArrow = "uArr";
var DoubleUpDownArrow = "#x21D5";
var DoubleVerticalBar = "#x2225";
var DownArrow = "darr";
var DownArrowBar = "#x2913";
var DownArrowUpArrow = "#x21F5";
var DownBreve = "#x311";
var DownLeftRightVector = "#x2950";
var DownLeftTeeVector = "#x295E";
var DownLeftVector = "#x21BD";
var DownLeftVectorBar = "#x2956";
var DownRightTeeVector = "#x295F";
var DownRightVector = "#x21C1";
var DownRightVectorBar = "#x2957";
var DownTee = "#x22A4";
var DownTeeArrow = "#x21A7";
var Downarrow = "dArr";
var Dscr = "#x1D49F";
var Dstrok = "#x110";
var ENG = "#x14A";
var Ecaron = "#x11A";
var Ecy = "#x42D";
var Edot = "#x116";
var Efr = "#x1D508";
var Element = "#x2208";
var Emacr = "#x112";
var EmptySmallSquare = "#x25FB";
var EmptyVerySmallSquare = "#x25AB";
var Eogon = "#x118";
var Eopf = "#x1D53C";
var Equal = "#x2A75";
var EqualTilde = "#x2242";
var Equilibrium = "#x21CC";
var Escr = "#x2130";
var Esim = "#x2A73";
var Exists = "exist";
var ExponentialE = "#x2147";
var Fcy = "#x424";
var Ffr = "#x1D509";
var FilledSmallSquare = "#x25FC";
var FilledVerySmallSquare = "#x25AA";
var Fopf = "#x1D53D";
var ForAll = "forall";
var Fouriertrf = "#x2131";
var Fscr = "#x2131";
var GJcy = "#x403";
var GT = "gt";
var Gammad = "#x3DC";
var Gbreve = "#x11E";
var Gcedil = "#x122";
var Gcirc = "#x11C";
var Gcy = "#x413";
var Gdot = "#x120";
var Gfr = "#x1D50A";
var Gg = "#x22D9";
var Gopf = "#x1D53E";
var GreaterEqual = "ge";
var GreaterEqualLess = "#x22DB";
var GreaterFullEqual = "#x2267";
var GreaterGreater = "#x2AA2";
var GreaterLess = "#x2277";
var GreaterSlantEqual = "#x2A7E";
var GreaterTilde = "#x2273";
var Gscr = "#x1D4A2";
var Gt = "#x226B";
var HARDcy = "#x42A";
var Hacek = "#x2C7";
var Hcirc = "#x124";
var Hfr = "#x210C";
var HilbertSpace = "#x210B";
var Hopf = "#x210D";
var HorizontalLine = "#x2500";
var Hscr = "#x210B";
var Hstrok = "#x126";
var HumpDownHump = "#x224E";
var HumpEqual = "#x224F";
var IEcy = "#x415";
var IJlig = "#x132";
var IOcy = "#x401";
var Icy = "#x418";
var Idot = "#x130";
var Ifr = "#x2111";
var Im = "#x2111";
var Imacr = "#x12A";
var ImaginaryI = "#x2148";
var Implies = "rArr";
var Int = "#x222C";
var Integral = "int";
var Intersection = "#x22C2";
var InvisibleComma = "#x2063";
var InvisibleTimes = "#x2062";
var Iogon = "#x12E";
var Iopf = "#x1D540";
var Iscr = "#x2110";
var Itilde = "#x128";
var Iukcy = "#x406";
var Jcirc = "#x134";
var Jcy = "#x419";
var Jfr = "#x1D50D";
var Jopf = "#x1D541";
var Jscr = "#x1D4A5";
var Jsercy = "#x408";
var Jukcy = "#x404";
var KHcy = "#x425";
var KJcy = "#x40C";
var Kcedil = "#x136";
var Kcy = "#x41A";
var Kfr = "#x1D50E";
var Kopf = "#x1D542";
var Kscr = "#x1D4A6";
var LJcy = "#x409";
var LT = "lt";
var Lacute = "#x139";
var Lang = "#x27EA";
var Laplacetrf = "#x2112";
var Larr = "#x219E";
var Lcaron = "#x13D";
var Lcedil = "#x13B";
var Lcy = "#x41B";
var LeftAngleBracket = "lang";
var LeftArrow = "larr";
var LeftArrowBar = "#x21E4";
var LeftArrowRightArrow = "#x21C6";
var LeftCeiling = "lceil";
var LeftDoubleBracket = "#x27E6";
var LeftDownTeeVector = "#x2961";
var LeftDownVector = "#x21C3";
var LeftDownVectorBar = "#x2959";
var LeftFloor = "lfloor";
var LeftRightArrow = "harr";
var LeftRightVector = "#x294E";
var LeftTee = "#x22A3";
var LeftTeeArrow = "#x21A4";
var LeftTeeVector = "#x295A";
var LeftTriangle = "#x22B2";
var LeftTriangleBar = "#x29CF";
var LeftTriangleEqual = "#x22B4";
var LeftUpDownVector = "#x2951";
var LeftUpTeeVector = "#x2960";
var LeftUpVector = "#x21BF";
var LeftUpVectorBar = "#x2958";
var LeftVector = "#x21BC";
var LeftVectorBar = "#x2952";
var Leftarrow = "lArr";
var Leftrightarrow = "#x21D4";
var LessEqualGreater = "#x22DA";
var LessFullEqual = "#x2266";
var LessGreater = "#x2276";
var LessLess = "#x2AA1";
var LessSlantEqual = "#x2A7D";
var LessTilde = "#x2272";
var Lfr = "#x1D50F";
var Ll = "#x22D8";
var Lleftarrow = "#x21DA";
var Lmidot = "#x13F";
var LongLeftArrow = "#x27F5";
var LongLeftRightArrow = "#x27F7";
var LongRightArrow = "#x27F6";
var Longleftarrow = "#x27F8";
var Longleftrightarrow = "#x27FA";
var Longrightarrow = "#x27F9";
var Lopf = "#x1D543";
var LowerLeftArrow = "#x2199";
var LowerRightArrow = "#x2198";
var Lscr = "#x2112";
var Lsh = "#x21B0";
var Lstrok = "#x141";
var Lt = "#x226A";
var Mcy = "#x41C";
var MediumSpace = "#x205F";
var Mellintrf = "#x2133";
var Mfr = "#x1D510";
var MinusPlus = "#x2213";
var Mopf = "#x1D544";
var Mscr = "#x2133";
var NJcy = "#x40A";
var Nacute = "#x143";
var Ncaron = "#x147";
var Ncedil = "#x145";
var Ncy = "#x41D";
var NegativeMediumSpace = "#x200B";
var NegativeThickSpace = "#x200B";
var NegativeThinSpace = "#x200B";
var NegativeVeryThinSpace = "#x200B";
var NestedGreaterGreater = "#x226B";
var NestedLessLess = "#x226A";
var Nfr = "#x1D511";
var NoBreak = "#x2060";
var NonBreakingSpace = "nbsp";
var Nopf = "#x2115";
var Not = "#x2AEC";
var NotCongruent = "#x2262";
var NotCupCap = "#x226D";
var NotDoubleVerticalBar = "#x2226";
var NotElement = "notin";
var NotEqual = "ne";
var NotEqualTilde = "#x2242;&#x338";
var NotExists = "#x2204";
var NotGreater = "#x226F";
var NotGreaterEqual = "#x2271";
var NotGreaterFullEqual = "#x2267;&#x338";
var NotGreaterGreater = "#x226B;&#x338";
var NotGreaterLess = "#x2279";
var NotGreaterSlantEqual = "#x2A7E;&#x338";
var NotGreaterTilde = "#x2275";
var NotHumpDownHump = "#x224E;&#x338";
var NotHumpEqual = "#x224F;&#x338";
var NotLeftTriangle = "#x22EA";
var NotLeftTriangleBar = "#x29CF;&#x338";
var NotLeftTriangleEqual = "#x22EC";
var NotLess = "#x226E";
var NotLessEqual = "#x2270";
var NotLessGreater = "#x2278";
var NotLessLess = "#x226A;&#x338";
var NotLessSlantEqual = "#x2A7D;&#x338";
var NotLessTilde = "#x2274";
var NotNestedGreaterGreater = "#x2AA2;&#x338";
var NotNestedLessLess = "#x2AA1;&#x338";
var NotPrecedes = "#x2280";
var NotPrecedesEqual = "#x2AAF;&#x338";
var NotPrecedesSlantEqual = "#x22E0";
var NotReverseElement = "#x220C";
var NotRightTriangle = "#x22EB";
var NotRightTriangleBar = "#x29D0;&#x338";
var NotRightTriangleEqual = "#x22ED";
var NotSquareSubset = "#x228F;&#x338";
var NotSquareSubsetEqual = "#x22E2";
var NotSquareSuperset = "#x2290;&#x338";
var NotSquareSupersetEqual = "#x22E3";
var NotSubset = "#x2282;&#x20D2";
var NotSubsetEqual = "#x2288";
var NotSucceeds = "#x2281";
var NotSucceedsEqual = "#x2AB0;&#x338";
var NotSucceedsSlantEqual = "#x22E1";
var NotSucceedsTilde = "#x227F;&#x338";
var NotSuperset = "#x2283;&#x20D2";
var NotSupersetEqual = "#x2289";
var NotTilde = "#x2241";
var NotTildeEqual = "#x2244";
var NotTildeFullEqual = "#x2247";
var NotTildeTilde = "#x2249";
var NotVerticalBar = "#x2224";
var Nscr = "#x1D4A9";
var Ocy = "#x41E";
var Odblac = "#x150";
var Ofr = "#x1D512";
var Omacr = "#x14C";
var Oopf = "#x1D546";
var OpenCurlyDoubleQuote = "ldquo";
var OpenCurlyQuote = "lsquo";
var Or = "#x2A54";
var Oscr = "#x1D4AA";
var Otimes = "#x2A37";
var OverBar = "oline";
var OverBrace = "#x23DE";
var OverBracket = "#x23B4";
var OverParenthesis = "#x23DC";
var PartialD = "part";
var Pcy = "#x41F";
var Pfr = "#x1D513";
var PlusMinus = "#xB1";
var Poincareplane = "#x210C";
var Popf = "#x2119";
var Pr = "#x2ABB";
var Precedes = "#x227A";
var PrecedesEqual = "#x2AAF";
var PrecedesSlantEqual = "#x227C";
var PrecedesTilde = "#x227E";
var Product = "prod";
var Proportion = "#x2237";
var Proportional = "prop";
var Pscr = "#x1D4AB";
var QUOT = "quot";
var Qfr = "#x1D514";
var Qopf = "#x211A";
var Qscr = "#x1D4AC";
var RBarr = "#x2910";
var REG = "reg";
var Racute = "#x154";
var Rang = "#x27EB";
var Rarr = "#x21A0";
var Rarrtl = "#x2916";
var Rcaron = "#x158";
var Rcedil = "#x156";
var Rcy = "#x420";
var Re = "#x211C";
var ReverseElement = "ni";
var ReverseEquilibrium = "#x21CB";
var ReverseUpEquilibrium = "#x296F";
var Rfr = "#x211C";
var RightAngleBracket = "rang";
var RightArrow = "rarr";
var RightArrowBar = "#x21E5";
var RightArrowLeftArrow = "#x21C4";
var RightCeiling = "rceil";
var RightDoubleBracket = "#x27E7";
var RightDownTeeVector = "#x295D";
var RightDownVector = "#x21C2";
var RightDownVectorBar = "#x2955";
var RightFloor = "rfloor";
var RightTee = "#x22A2";
var RightTeeArrow = "#x21A6";
var RightTeeVector = "#x295B";
var RightTriangle = "#x22B3";
var RightTriangleBar = "#x29D0";
var RightTriangleEqual = "#x22B5";
var RightUpDownVector = "#x294F";
var RightUpTeeVector = "#x295C";
var RightUpVector = "#x21BE";
var RightUpVectorBar = "#x2954";
var RightVector = "#x21C0";
var RightVectorBar = "#x2953";
var Rightarrow = "rArr";
var Ropf = "#x211D";
var RoundImplies = "#x2970";
var Rrightarrow = "#x21DB";
var Rscr = "#x211B";
var Rsh = "#x21B1";
var RuleDelayed = "#x29F4";
var SHCHcy = "#x429";
var SHcy = "#x428";
var SOFTcy = "#x42C";
var Sacute = "#x15A";
var Sc = "#x2ABC";
var Scedil = "#x15E";
var Scirc = "#x15C";
var Scy = "#x421";
var Sfr = "#x1D516";
var ShortDownArrow = "darr";
var ShortLeftArrow = "larr";
var ShortRightArrow = "rarr";
var ShortUpArrow = "uarr";
var SmallCircle = "#x2218";
var Sopf = "#x1D54A";
var Sqrt = "#x221A";
var Square = "#x25A1";
var SquareIntersection = "#x2293";
var SquareSubset = "#x228F";
var SquareSubsetEqual = "#x2291";
var SquareSuperset = "#x2290";
var SquareSupersetEqual = "#x2292";
var SquareUnion = "#x2294";
var Sscr = "#x1D4AE";
var Star = "#x22C6";
var Sub = "#x22D0";
var Subset = "#x22D0";
var SubsetEqual = "sube";
var Succeeds = "#x227B";
var SucceedsEqual = "#x2AB0";
var SucceedsSlantEqual = "#x227D";
var SucceedsTilde = "#x227F";
var SuchThat = "ni";
var Sum = "sum";
var Sup = "#x22D1";
var Superset = "sup";
var SupersetEqual = "supe";
var Supset = "#x22D1";
var TRADE = "trade";
var TSHcy = "#x40B";
var TScy = "#x426";
var Tab = "#x9";
var Tcaron = "#x164";
var Tcedil = "#x162";
var Tcy = "#x422";
var Tfr = "#x1D517";
var Therefore = "there4";
var ThickSpace = "#x205F;&#x200A";
var ThinSpace = "thinsp";
var Tilde = "sim";
var TildeEqual = "#x2243";
var TildeFullEqual = "cong";
var TildeTilde = "#x2248";
var Topf = "#x1D54B";
var TripleDot = "#x20DB";
var Tscr = "#x1D4AF";
var Tstrok = "#x166";
var Uarr = "#x219F";
var Uarrocir = "#x2949";
var Ubrcy = "#x40E";
var Ubreve = "#x16C";
var Ucy = "#x423";
var Udblac = "#x170";
var Ufr = "#x1D518";
var Umacr = "#x16A";
var UnderBrace = "#x23DF";
var UnderBracket = "#x23B5";
var UnderParenthesis = "#x23DD";
var Union = "#x22C3";
var UnionPlus = "#x228E";
var Uogon = "#x172";
var Uopf = "#x1D54C";
var UpArrow = "uarr";
var UpArrowBar = "#x2912";
var UpArrowDownArrow = "#x21C5";
var UpDownArrow = "#x2195";
var UpEquilibrium = "#x296E";
var UpTee = "#x22A5";
var UpTeeArrow = "#x21A5";
var Uparrow = "uArr";
var Updownarrow = "#x21D5";
var UpperLeftArrow = "#x2196";
var UpperRightArrow = "#x2197";
var Upsi = "#x3D2";
var Uring = "#x16E";
var Uscr = "#x1D4B0";
var Utilde = "#x168";
var VDash = "#x22AB";
var Vbar = "#x2AEB";
var Vcy = "#x412";
var Vdash = "#x22A9";
var Vdashl = "#x2AE6";
var Vee = "#x22C1";
var Verbar = "#x2016";
var Vert = "#x2016";
var VerticalBar = "#x2223";
var VerticalSeparator = "#x2758";
var VerticalTilde = "#x2240";
var VeryThinSpace = "#x200A";
var Vfr = "#x1D519";
var Vopf = "#x1D54D";
var Vscr = "#x1D4B1";
var Vvdash = "#x22AA";
var Wcirc = "#x174";
var Wedge = "#x22C0";
var Wfr = "#x1D51A";
var Wopf = "#x1D54E";
var Wscr = "#x1D4B2";
var Xfr = "#x1D51B";
var Xopf = "#x1D54F";
var Xscr = "#x1D4B3";
var YAcy = "#x42F";
var YIcy = "#x407";
var YUcy = "#x42E";
var Ycirc = "#x176";
var Ycy = "#x42B";
var Yfr = "#x1D51C";
var Yopf = "#x1D550";
var Yscr = "#x1D4B4";
var ZHcy = "#x416";
var Zacute = "#x179";
var Zcaron = "#x17D";
var Zcy = "#x417";
var Zdot = "#x17B";
var ZeroWidthSpace = "#x200B";
var Zfr = "#x2128";
var Zopf = "#x2124";
var Zscr = "#x1D4B5";
var abreve = "#x103";
var ac = "#x223E";
var acE = "#x223E;&#x333";
var acd = "#x223F";
var acy = "#x430";
var af = "#x2061";
var afr = "#x1D51E";
var aleph = "#x2135";
var amacr = "#x101";
var amalg = "#x2A3F";
var andand = "#x2A55";
var andd = "#x2A5C";
var andslope = "#x2A58";
var andv = "#x2A5A";
var ange = "#x29A4";
var angle = "ang";
var angmsd = "#x2221";
var angmsdaa = "#x29A8";
var angmsdab = "#x29A9";
var angmsdac = "#x29AA";
var angmsdad = "#x29AB";
var angmsdae = "#x29AC";
var angmsdaf = "#x29AD";
var angmsdag = "#x29AE";
var angmsdah = "#x29AF";
var angrt = "#x221F";
var angrtvb = "#x22BE";
var angrtvbd = "#x299D";
var angsph = "#x2222";
var angst = "#xC5";
var angzarr = "#x237C";
var aogon = "#x105";
var aopf = "#x1D552";
var ap = "#x2248";
var apE = "#x2A70";
var apacir = "#x2A6F";
var ape = "#x224A";
var apid = "#x224B";
var approx = "#x2248";
var approxeq = "#x224A";
var ascr = "#x1D4B6";
var asympeq = "#x224D";
var awconint = "#x2233";
var awint = "#x2A11";
var bNot = "#x2AED";
var backcong = "#x224C";
var backepsilon = "#x3F6";
var backprime = "#x2035";
var backsim = "#x223D";
var backsimeq = "#x22CD";
var barvee = "#x22BD";
var barwed = "#x2305";
var barwedge = "#x2305";
var bbrk = "#x23B5";
var bbrktbrk = "#x23B6";
var bcong = "#x224C";
var bcy = "#x431";
var becaus = "#x2235";
var because = "#x2235";
var bemptyv = "#x29B0";
var bepsi = "#x3F6";
var bernou = "#x212C";
var beth = "#x2136";
var between = "#x226C";
var bfr = "#x1D51F";
var bigcap = "#x22C2";
var bigcirc = "#x25EF";
var bigcup = "#x22C3";
var bigodot = "#x2A00";
var bigoplus = "#x2A01";
var bigotimes = "#x2A02";
var bigsqcup = "#x2A06";
var bigstar = "#x2605";
var bigtriangledown = "#x25BD";
var bigtriangleup = "#x25B3";
var biguplus = "#x2A04";
var bigvee = "#x22C1";
var bigwedge = "#x22C0";
var bkarow = "#x290D";
var blacklozenge = "#x29EB";
var blacksquare = "#x25AA";
var blacktriangle = "#x25B4";
var blacktriangledown = "#x25BE";
var blacktriangleleft = "#x25C2";
var blacktriangleright = "#x25B8";
var blank = "#x2423";
var blk12 = "#x2592";
var blk14 = "#x2591";
var blk34 = "#x2593";
var block = "#x2588";
var bne = "&#x20E5";
var bnequiv = "#x2261;&#x20E5";
var bnot = "#x2310";
var bopf = "#x1D553";
var bot = "#x22A5";
var bottom = "#x22A5";
var bowtie = "#x22C8";
var boxDL = "#x2557";
var boxDR = "#x2554";
var boxDl = "#x2556";
var boxDr = "#x2553";
var boxH = "#x2550";
var boxHD = "#x2566";
var boxHU = "#x2569";
var boxHd = "#x2564";
var boxHu = "#x2567";
var boxUL = "#x255D";
var boxUR = "#x255A";
var boxUl = "#x255C";
var boxUr = "#x2559";
var boxV = "#x2551";
var boxVH = "#x256C";
var boxVL = "#x2563";
var boxVR = "#x2560";
var boxVh = "#x256B";
var boxVl = "#x2562";
var boxVr = "#x255F";
var boxbox = "#x29C9";
var boxdL = "#x2555";
var boxdR = "#x2552";
var boxdl = "#x2510";
var boxdr = "#x250C";
var boxh = "#x2500";
var boxhD = "#x2565";
var boxhU = "#x2568";
var boxhd = "#x252C";
var boxhu = "#x2534";
var boxminus = "#x229F";
var boxplus = "#x229E";
var boxtimes = "#x22A0";
var boxuL = "#x255B";
var boxuR = "#x2558";
var boxul = "#x2518";
var boxur = "#x2514";
var boxv = "#x2502";
var boxvH = "#x256A";
var boxvL = "#x2561";
var boxvR = "#x255E";
var boxvh = "#x253C";
var boxvl = "#x2524";
var boxvr = "#x251C";
var bprime = "#x2035";
var breve = "#x2D8";
var bscr = "#x1D4B7";
var bsemi = "#x204F";
var bsim = "#x223D";
var bsime = "#x22CD";
var bsolb = "#x29C5";
var bsolhsub = "#x27C8";
var bullet = "bull";
var bump = "#x224E";
var bumpE = "#x2AAE";
var bumpe = "#x224F";
var bumpeq = "#x224F";
var cacute = "#x107";
var capand = "#x2A44";
var capbrcup = "#x2A49";
var capcap = "#x2A4B";
var capcup = "#x2A47";
var capdot = "#x2A40";
var caps = "#x2229;&#xFE00";
var caret = "#x2041";
var caron = "#x2C7";
var ccaps = "#x2A4D";
var ccaron = "#x10D";
var ccirc = "#x109";
var ccups = "#x2A4C";
var ccupssm = "#x2A50";
var cdot = "#x10B";
var cemptyv = "#x29B2";
var centerdot = "middot";
var cfr = "#x1D520";
var chcy = "#x447";
var check = "#x2713";
var checkmark = "#x2713";
var cir = "#x25CB";
var cirE = "#x29C3";
var circeq = "#x2257";
var circlearrowleft = "#x21BA";
var circlearrowright = "#x21BB";
var circledR = "reg";
var circledS = "#x24C8";
var circledast = "#x229B";
var circledcirc = "#x229A";
var circleddash = "#x229D";
var cire = "#x2257";
var cirfnint = "#x2A10";
var cirmid = "#x2AEF";
var cirscir = "#x29C2";
var clubsuit = "clubs";
var colone = "#x2254";
var coloneq = "#x2254";
var comp = "#x2201";
var compfn = "#x2218";
var complement = "#x2201";
var complexes = "#x2102";
var congdot = "#x2A6D";
var conint = "#x222E";
var copf = "#x1D554";
var coprod = "#x2210";
var copysr = "#x2117";
var cross = "#x2717";
var cscr = "#x1D4B8";
var csub = "#x2ACF";
var csube = "#x2AD1";
var csup = "#x2AD0";
var csupe = "#x2AD2";
var ctdot = "#x22EF";
var cudarrl = "#x2938";
var cudarrr = "#x2935";
var cuepr = "#x22DE";
var cuesc = "#x22DF";
var cularr = "#x21B6";
var cularrp = "#x293D";
var cupbrcap = "#x2A48";
var cupcap = "#x2A46";
var cupcup = "#x2A4A";
var cupdot = "#x228D";
var cupor = "#x2A45";
var cups = "#x222A;&#xFE00";
var curarr = "#x21B7";
var curarrm = "#x293C";
var curlyeqprec = "#x22DE";
var curlyeqsucc = "#x22DF";
var curlyvee = "#x22CE";
var curlywedge = "#x22CF";
var curvearrowleft = "#x21B6";
var curvearrowright = "#x21B7";
var cuvee = "#x22CE";
var cuwed = "#x22CF";
var cwconint = "#x2232";
var cwint = "#x2231";
var cylcty = "#x232D";
var dHar = "#x2965";
var daleth = "#x2138";
var dash = "#x2010";
var dashv = "#x22A3";
var dbkarow = "#x290F";
var dblac = "#x2DD";
var dcaron = "#x10F";
var dcy = "#x434";
var dd = "#x2146";
var ddagger = "Dagger";
var ddarr = "#x21CA";
var ddotseq = "#x2A77";
var demptyv = "#x29B1";
var dfisht = "#x297F";
var dfr = "#x1D521";
var dharl = "#x21C3";
var dharr = "#x21C2";
var diam = "#x22C4";
var diamond = "#x22C4";
var diamondsuit = "diams";
var die = "#xA8";
var digamma = "#x3DD";
var disin = "#x22F2";
var div = "#xF7";
var divideontimes = "#x22C7";
var divonx = "#x22C7";
var djcy = "#x452";
var dlcorn = "#x231E";
var dlcrop = "#x230D";
var dopf = "#x1D555";
var dot = "#x2D9";
var doteq = "#x2250";
var doteqdot = "#x2251";
var dotminus = "#x2238";
var dotplus = "#x2214";
var dotsquare = "#x22A1";
var doublebarwedge = "#x2306";
var downarrow = "darr";
var downdownarrows = "#x21CA";
var downharpoonleft = "#x21C3";
var downharpoonright = "#x21C2";
var drbkarow = "#x2910";
var drcorn = "#x231F";
var drcrop = "#x230C";
var dscr = "#x1D4B9";
var dscy = "#x455";
var dsol = "#x29F6";
var dstrok = "#x111";
var dtdot = "#x22F1";
var dtri = "#x25BF";
var dtrif = "#x25BE";
var duarr = "#x21F5";
var duhar = "#x296F";
var dwangle = "#x29A6";
var dzcy = "#x45F";
var dzigrarr = "#x27FF";
var eDDot = "#x2A77";
var eDot = "#x2251";
var easter = "#x2A6E";
var ecaron = "#x11B";
var ecir = "#x2256";
var ecolon = "#x2255";
var ecy = "#x44D";
var edot = "#x117";
var ee = "#x2147";
var efDot = "#x2252";
var efr = "#x1D522";
var eg = "#x2A9A";
var egs = "#x2A96";
var egsdot = "#x2A98";
var el = "#x2A99";
var elinters = "#x23E7";
var ell = "#x2113";
var els = "#x2A95";
var elsdot = "#x2A97";
var emacr = "#x113";
var emptyset = "empty";
var emptyv = "empty";
var emsp13 = "#x2004";
var emsp14 = "#x2005";
var eng = "#x14B";
var eogon = "#x119";
var eopf = "#x1D556";
var epar = "#x22D5";
var eparsl = "#x29E3";
var eplus = "#x2A71";
var epsi = "#x3B5";
var epsiv = "#x3F5";
var eqcirc = "#x2256";
var eqcolon = "#x2255";
var eqsim = "#x2242";
var eqslantgtr = "#x2A96";
var eqslantless = "#x2A95";
var equest = "#x225F";
var equivDD = "#x2A78";
var eqvparsl = "#x29E5";
var erDot = "#x2253";
var erarr = "#x2971";
var escr = "#x212F";
var esdot = "#x2250";
var esim = "#x2242";
var expectation = "#x2130";
var exponentiale = "#x2147";
var fallingdotseq = "#x2252";
var fcy = "#x444";
var female = "#x2640";
var ffilig = "#xFB03";
var fflig = "#xFB00";
var ffllig = "#xFB04";
var ffr = "#x1D523";
var filig = "#xFB01";
var flat = "#x266D";
var fllig = "#xFB02";
var fltns = "#x25B1";
var fopf = "#x1D557";
var fork = "#x22D4";
var forkv = "#x2AD9";
var fpartint = "#x2A0D";
var frac13 = "#x2153";
var frac15 = "#x2155";
var frac16 = "#x2159";
var frac18 = "#x215B";
var frac23 = "#x2154";
var frac25 = "#x2156";
var frac35 = "#x2157";
var frac38 = "#x215C";
var frac45 = "#x2158";
var frac56 = "#x215A";
var frac58 = "#x215D";
var frac78 = "#x215E";
var frown = "#x2322";
var fscr = "#x1D4BB";
var gE = "#x2267";
var gEl = "#x2A8C";
var gacute = "#x1F5";
var gammad = "#x3DD";
var gap = "#x2A86";
var gbreve = "#x11F";
var gcirc = "#x11D";
var gcy = "#x433";
var gdot = "#x121";
var gel = "#x22DB";
var geq = "ge";
var geqq = "#x2267";
var geqslant = "#x2A7E";
var ges = "#x2A7E";
var gescc = "#x2AA9";
var gesdot = "#x2A80";
var gesdoto = "#x2A82";
var gesdotol = "#x2A84";
var gesl = "#x22DB;&#xFE00";
var gesles = "#x2A94";
var gfr = "#x1D524";
var gg = "#x226B";
var ggg = "#x22D9";
var gimel = "#x2137";
var gjcy = "#x453";
var gl = "#x2277";
var glE = "#x2A92";
var gla = "#x2AA5";
var glj = "#x2AA4";
var gnE = "#x2269";
var gnap = "#x2A8A";
var gnapprox = "#x2A8A";
var gne = "#x2A88";
var gneq = "#x2A88";
var gneqq = "#x2269";
var gnsim = "#x22E7";
var gopf = "#x1D558";
var grave = "#x60";
var gscr = "#x210A";
var gsim = "#x2273";
var gsime = "#x2A8E";
var gsiml = "#x2A90";
var gtcc = "#x2AA7";
var gtcir = "#x2A7A";
var gtdot = "#x22D7";
var gtlPar = "#x2995";
var gtquest = "#x2A7C";
var gtrapprox = "#x2A86";
var gtrarr = "#x2978";
var gtrdot = "#x22D7";
var gtreqless = "#x22DB";
var gtreqqless = "#x2A8C";
var gtrless = "#x2277";
var gtrsim = "#x2273";
var gvertneqq = "#x2269;&#xFE00";
var gvnE = "#x2269;&#xFE00";
var hairsp = "#x200A";
var half = "#xBD";
var hamilt = "#x210B";
var hardcy = "#x44A";
var harrcir = "#x2948";
var harrw = "#x21AD";
var hbar = "#x210F";
var hcirc = "#x125";
var heartsuit = "hearts";
var hercon = "#x22B9";
var hfr = "#x1D525";
var hksearow = "#x2925";
var hkswarow = "#x2926";
var hoarr = "#x21FF";
var homtht = "#x223B";
var hookleftarrow = "#x21A9";
var hookrightarrow = "#x21AA";
var hopf = "#x1D559";
var horbar = "#x2015";
var hscr = "#x1D4BD";
var hslash = "#x210F";
var hstrok = "#x127";
var hybull = "#x2043";
var hyphen = "#x2010";
var ic = "#x2063";
var icy = "#x438";
var iecy = "#x435";
var iff = "#x21D4";
var ifr = "#x1D526";
var ii = "#x2148";
var iiiint = "#x2A0C";
var iiint = "#x222D";
var iinfin = "#x29DC";
var iiota = "#x2129";
var ijlig = "#x133";
var imacr = "#x12B";
var imagline = "#x2110";
var imagpart = "#x2111";
var imath = "#x131";
var imof = "#x22B7";
var imped = "#x1B5";
var incare = "#x2105";
var infintie = "#x29DD";
var inodot = "#x131";
var intcal = "#x22BA";
var integers = "#x2124";
var intercal = "#x22BA";
var intlarhk = "#x2A17";
var intprod = "#x2A3C";
var iocy = "#x451";
var iogon = "#x12F";
var iopf = "#x1D55A";
var iprod = "#x2A3C";
var iscr = "#x1D4BE";
var isinE = "#x22F9";
var isindot = "#x22F5";
var isins = "#x22F4";
var isinsv = "#x22F3";
var isinv = "#x2208";
var it = "#x2062";
var itilde = "#x129";
var iukcy = "#x456";
var jcirc = "#x135";
var jcy = "#x439";
var jfr = "#x1D527";
var jmath = "#x237";
var jopf = "#x1D55B";
var jscr = "#x1D4BF";
var jsercy = "#x458";
var jukcy = "#x454";
var kappav = "#x3F0";
var kcedil = "#x137";
var kcy = "#x43A";
var kfr = "#x1D528";
var kgreen = "#x138";
var khcy = "#x445";
var kjcy = "#x45C";
var kopf = "#x1D55C";
var kscr = "#x1D4C0";
var lAarr = "#x21DA";
var lAtail = "#x291B";
var lBarr = "#x290E";
var lE = "#x2266";
var lEg = "#x2A8B";
var lHar = "#x2962";
var lacute = "#x13A";
var laemptyv = "#x29B4";
var lagran = "#x2112";
var langd = "#x2991";
var langle = "lang";
var lap = "#x2A85";
var larrb = "#x21E4";
var larrbfs = "#x291F";
var larrfs = "#x291D";
var larrhk = "#x21A9";
var larrlp = "#x21AB";
var larrpl = "#x2939";
var larrsim = "#x2973";
var larrtl = "#x21A2";
var lat = "#x2AAB";
var latail = "#x2919";
var late = "#x2AAD";
var lates = "#x2AAD;&#xFE00";
var lbarr = "#x290C";
var lbbrk = "#x2772";
var lbrace = "{";
var lbrack = "[";
var lbrke = "#x298B";
var lbrksld = "#x298F";
var lbrkslu = "#x298D";
var lcaron = "#x13E";
var lcedil = "#x13C";
var lcub = "{";
var lcy = "#x43B";
var ldca = "#x2936";
var ldquor = "bdquo";
var ldrdhar = "#x2967";
var ldrushar = "#x294B";
var ldsh = "#x21B2";
var leftarrow = "larr";
var leftarrowtail = "#x21A2";
var leftharpoondown = "#x21BD";
var leftharpoonup = "#x21BC";
var leftleftarrows = "#x21C7";
var leftrightarrow = "harr";
var leftrightarrows = "#x21C6";
var leftrightharpoons = "#x21CB";
var leftrightsquigarrow = "#x21AD";
var leftthreetimes = "#x22CB";
var leg = "#x22DA";
var leq = "le";
var leqq = "#x2266";
var leqslant = "#x2A7D";
var les = "#x2A7D";
var lescc = "#x2AA8";
var lesdot = "#x2A7F";
var lesdoto = "#x2A81";
var lesdotor = "#x2A83";
var lesg = "#x22DA;&#xFE00";
var lesges = "#x2A93";
var lessapprox = "#x2A85";
var lessdot = "#x22D6";
var lesseqgtr = "#x22DA";
var lesseqqgtr = "#x2A8B";
var lessgtr = "#x2276";
var lesssim = "#x2272";
var lfisht = "#x297C";
var lfr = "#x1D529";
var lg = "#x2276";
var lgE = "#x2A91";
var lhard = "#x21BD";
var lharu = "#x21BC";
var lharul = "#x296A";
var lhblk = "#x2584";
var ljcy = "#x459";
var ll = "#x226A";
var llarr = "#x21C7";
var llcorner = "#x231E";
var llhard = "#x296B";
var lltri = "#x25FA";
var lmidot = "#x140";
var lmoust = "#x23B0";
var lmoustache = "#x23B0";
var lnE = "#x2268";
var lnap = "#x2A89";
var lnapprox = "#x2A89";
var lne = "#x2A87";
var lneq = "#x2A87";
var lneqq = "#x2268";
var lnsim = "#x22E6";
var loang = "#x27EC";
var loarr = "#x21FD";
var lobrk = "#x27E6";
var longleftarrow = "#x27F5";
var longleftrightarrow = "#x27F7";
var longmapsto = "#x27FC";
var longrightarrow = "#x27F6";
var looparrowleft = "#x21AB";
var looparrowright = "#x21AC";
var lopar = "#x2985";
var lopf = "#x1D55D";
var loplus = "#x2A2D";
var lotimes = "#x2A34";
var lozenge = "loz";
var lozf = "#x29EB";
var lparlt = "#x2993";
var lrarr = "#x21C6";
var lrcorner = "#x231F";
var lrhar = "#x21CB";
var lrhard = "#x296D";
var lrtri = "#x22BF";
var lscr = "#x1D4C1";
var lsh = "#x21B0";
var lsim = "#x2272";
var lsime = "#x2A8D";
var lsimg = "#x2A8F";
var lsquor = "sbquo";
var lstrok = "#x142";
var ltcc = "#x2AA6";
var ltcir = "#x2A79";
var ltdot = "#x22D6";
var lthree = "#x22CB";
var ltimes = "#x22C9";
var ltlarr = "#x2976";
var ltquest = "#x2A7B";
var ltrPar = "#x2996";
var ltri = "#x25C3";
var ltrie = "#x22B4";
var ltrif = "#x25C2";
var lurdshar = "#x294A";
var luruhar = "#x2966";
var lvertneqq = "#x2268;&#xFE00";
var lvnE = "#x2268;&#xFE00";
var mDDot = "#x223A";
var male = "#x2642";
var malt = "#x2720";
var maltese = "#x2720";
var map = "#x21A6";
var mapsto = "#x21A6";
var mapstodown = "#x21A7";
var mapstoleft = "#x21A4";
var mapstoup = "#x21A5";
var marker = "#x25AE";
var mcomma = "#x2A29";
var mcy = "#x43C";
var measuredangle = "#x2221";
var mfr = "#x1D52A";
var mho = "#x2127";
var mid = "#x2223";
var midcir = "#x2AF0";
var minusb = "#x229F";
var minusd = "#x2238";
var minusdu = "#x2A2A";
var mlcp = "#x2ADB";
var mldr = "#x2026";
var mnplus = "#x2213";
var models = "#x22A7";
var mopf = "#x1D55E";
var mp = "#x2213";
var mscr = "#x1D4C2";
var mstpos = "#x223E";
var multimap = "#x22B8";
var mumap = "#x22B8";
var nGg = "#x22D9;&#x338";
var nGt = "#x226B;&#x20D2";
var nGtv = "#x226B;&#x338";
var nLeftarrow = "#x21CD";
var nLeftrightarrow = "#x21CE";
var nLl = "#x22D8;&#x338";
var nLt = "#x226A;&#x20D2";
var nLtv = "#x226A;&#x338";
var nRightarrow = "#x21CF";
var nVDash = "#x22AF";
var nVdash = "#x22AE";
var nacute = "#x144";
var nang = "#x2220;&#x20D2";
var nap = "#x2249";
var napE = "#x2A70;&#x338";
var napid = "#x224B;&#x338";
var napos = "#x149";
var napprox = "#x2249";
var natur = "#x266E";
var natural = "#x266E";
var naturals = "#x2115";
var nbump = "#x224E;&#x338";
var nbumpe = "#x224F;&#x338";
var ncap = "#x2A43";
var ncaron = "#x148";
var ncedil = "#x146";
var ncong = "#x2247";
var ncongdot = "#x2A6D;&#x338";
var ncup = "#x2A42";
var ncy = "#x43D";
var neArr = "#x21D7";
var nearhk = "#x2924";
var nearr = "#x2197";
var nearrow = "#x2197";
var nedot = "#x2250;&#x338";
var nequiv = "#x2262";
var nesear = "#x2928";
var nesim = "#x2242;&#x338";
var nexist = "#x2204";
var nexists = "#x2204";
var nfr = "#x1D52B";
var ngE = "#x2267;&#x338";
var nge = "#x2271";
var ngeq = "#x2271";
var ngeqq = "#x2267;&#x338";
var ngeqslant = "#x2A7E;&#x338";
var nges = "#x2A7E;&#x338";
var ngsim = "#x2275";
var ngt = "#x226F";
var ngtr = "#x226F";
var nhArr = "#x21CE";
var nharr = "#x21AE";
var nhpar = "#x2AF2";
var nis = "#x22FC";
var nisd = "#x22FA";
var niv = "ni";
var njcy = "#x45A";
var nlArr = "#x21CD";
var nlE = "#x2266;&#x338";
var nlarr = "#x219A";
var nldr = "#x2025";
var nle = "#x2270";
var nleftarrow = "#x219A";
var nleftrightarrow = "#x21AE";
var nleq = "#x2270";
var nleqq = "#x2266;&#x338";
var nleqslant = "#x2A7D;&#x338";
var nles = "#x2A7D;&#x338";
var nless = "#x226E";
var nlsim = "#x2274";
var nlt = "#x226E";
var nltri = "#x22EA";
var nltrie = "#x22EC";
var nmid = "#x2224";
var nopf = "#x1D55F";
var notinE = "#x22F9;&#x338";
var notindot = "#x22F5;&#x338";
var notinva = "notin";
var notinvb = "#x22F7";
var notinvc = "#x22F6";
var notni = "#x220C";
var notniva = "#x220C";
var notnivb = "#x22FE";
var notnivc = "#x22FD";
var npar = "#x2226";
var nparallel = "#x2226";
var nparsl = "#x2AFD;&#x20E5";
var npart = "#x2202;&#x338";
var npolint = "#x2A14";
var npr = "#x2280";
var nprcue = "#x22E0";
var npre = "#x2AAF;&#x338";
var nprec = "#x2280";
var npreceq = "#x2AAF;&#x338";
var nrArr = "#x21CF";
var nrarr = "#x219B";
var nrarrc = "#x2933;&#x338";
var nrarrw = "#x219D;&#x338";
var nrightarrow = "#x219B";
var nrtri = "#x22EB";
var nrtrie = "#x22ED";
var nsc = "#x2281";
var nsccue = "#x22E1";
var nsce = "#x2AB0;&#x338";
var nscr = "#x1D4C3";
var nshortmid = "#x2224";
var nshortparallel = "#x2226";
var nsim = "#x2241";
var nsime = "#x2244";
var nsimeq = "#x2244";
var nsmid = "#x2224";
var nspar = "#x2226";
var nsqsube = "#x22E2";
var nsqsupe = "#x22E3";
var nsubE = "#x2AC5;&#x338";
var nsube = "#x2288";
var nsubset = "#x2282;&#x20D2";
var nsubseteq = "#x2288";
var nsubseteqq = "#x2AC5;&#x338";
var nsucc = "#x2281";
var nsucceq = "#x2AB0;&#x338";
var nsup = "#x2285";
var nsupE = "#x2AC6;&#x338";
var nsupe = "#x2289";
var nsupset = "#x2283;&#x20D2";
var nsupseteq = "#x2289";
var nsupseteqq = "#x2AC6;&#x338";
var ntgl = "#x2279";
var ntlg = "#x2278";
var ntriangleleft = "#x22EA";
var ntrianglelefteq = "#x22EC";
var ntriangleright = "#x22EB";
var ntrianglerighteq = "#x22ED";
var numero = "#x2116";
var numsp = "#x2007";
var nvDash = "#x22AD";
var nvHarr = "#x2904";
var nvap = "#x224D;&#x20D2";
var nvdash = "#x22AC";
var nvge = "#x2265;&#x20D2";
var nvgt = "#x3E;&#x20D2";
var nvinfin = "#x29DE";
var nvlArr = "#x2902";
var nvle = "#x2264;&#x20D2";
var nvlt = "#x3C;&#x20D2";
var nvltrie = "#x22B4;&#x20D2";
var nvrArr = "#x2903";
var nvrtrie = "#x22B5;&#x20D2";
var nvsim = "#x223C;&#x20D2";
var nwArr = "#x21D6";
var nwarhk = "#x2923";
var nwarr = "#x2196";
var nwarrow = "#x2196";
var nwnear = "#x2927";
var oS = "#x24C8";
var oast = "#x229B";
var ocir = "#x229A";
var ocy = "#x43E";
var odash = "#x229D";
var odblac = "#x151";
var odiv = "#x2A38";
var odot = "#x2299";
var odsold = "#x29BC";
var ofcir = "#x29BF";
var ofr = "#x1D52C";
var ogon = "#x2DB";
var ogt = "#x29C1";
var ohbar = "#x29B5";
var ohm = "#x3A9";
var oint = "#x222E";
var olarr = "#x21BA";
var olcir = "#x29BE";
var olcross = "#x29BB";
var olt = "#x29C0";
var omacr = "#x14D";
var omid = "#x29B6";
var ominus = "#x2296";
var oopf = "#x1D560";
var opar = "#x29B7";
var operp = "#x29B9";
var orarr = "#x21BB";
var ord = "#x2A5D";
var order = "#x2134";
var orderof = "#x2134";
var origof = "#x22B6";
var oror = "#x2A56";
var orslope = "#x2A57";
var orv = "#x2A5B";
var oscr = "#x2134";
var osol = "#x2298";
var otimesas = "#x2A36";
var ovbar = "#x233D";
var par = "#x2225";
var parallel = "#x2225";
var parsim = "#x2AF3";
var parsl = "#x2AFD";
var pcy = "#x43F";
var pertenk = "#x2031";
var pfr = "#x1D52D";
var phiv = "#x3D5";
var phmmat = "#x2133";
var phone = "#x260E";
var pitchfork = "#x22D4";
var planck = "#x210F";
var planckh = "#x210E";
var plankv = "#x210F";
var plusacir = "#x2A23";
var plusb = "#x229E";
var pluscir = "#x2A22";
var plusdo = "#x2214";
var plusdu = "#x2A25";
var pluse = "#x2A72";
var plussim = "#x2A26";
var plustwo = "#x2A27";
var pm = "#xB1";
var pointint = "#x2A15";
var popf = "#x1D561";
var pr = "#x227A";
var prE = "#x2AB3";
var prap = "#x2AB7";
var prcue = "#x227C";
var pre = "#x2AAF";
var prec = "#x227A";
var precapprox = "#x2AB7";
var preccurlyeq = "#x227C";
var preceq = "#x2AAF";
var precnapprox = "#x2AB9";
var precneqq = "#x2AB5";
var precnsim = "#x22E8";
var precsim = "#x227E";
var primes = "#x2119";
var prnE = "#x2AB5";
var prnap = "#x2AB9";
var prnsim = "#x22E8";
var profalar = "#x232E";
var profline = "#x2312";
var profsurf = "#x2313";
var propto = "prop";
var prsim = "#x227E";
var prurel = "#x22B0";
var pscr = "#x1D4C5";
var puncsp = "#x2008";
var qfr = "#x1D52E";
var qint = "#x2A0C";
var qopf = "#x1D562";
var qprime = "#x2057";
var qscr = "#x1D4C6";
var quaternions = "#x210D";
var quatint = "#x2A16";
var questeq = "#x225F";
var rAarr = "#x21DB";
var rAtail = "#x291C";
var rBarr = "#x290F";
var rHar = "#x2964";
var race = "#x223D;&#x331";
var racute = "#x155";
var raemptyv = "#x29B3";
var rangd = "#x2992";
var range = "#x29A5";
var rangle = "rang";
var rarrap = "#x2975";
var rarrb = "#x21E5";
var rarrbfs = "#x2920";
var rarrc = "#x2933";
var rarrfs = "#x291E";
var rarrhk = "#x21AA";
var rarrlp = "#x21AC";
var rarrpl = "#x2945";
var rarrsim = "#x2974";
var rarrtl = "#x21A3";
var rarrw = "#x219D";
var ratail = "#x291A";
var ratio = "#x2236";
var rationals = "#x211A";
var rbarr = "#x290D";
var rbbrk = "#x2773";
var rbrke = "#x298C";
var rbrksld = "#x298E";
var rbrkslu = "#x2990";
var rcaron = "#x159";
var rcedil = "#x157";
var rcy = "#x440";
var rdca = "#x2937";
var rdldhar = "#x2969";
var rdquor = "rdquo";
var rdsh = "#x21B3";
var realine = "#x211B";
var realpart = "#x211C";
var reals = "#x211D";
var rect = "#x25AD";
var rfisht = "#x297D";
var rfr = "#x1D52F";
var rhard = "#x21C1";
var rharu = "#x21C0";
var rharul = "#x296C";
var rhov = "#x3F1";
var rightarrow = "rarr";
var rightarrowtail = "#x21A3";
var rightharpoondown = "#x21C1";
var rightharpoonup = "#x21C0";
var rightleftarrows = "#x21C4";
var rightleftharpoons = "#x21CC";
var rightrightarrows = "#x21C9";
var rightsquigarrow = "#x219D";
var rightthreetimes = "#x22CC";
var ring = "#x2DA";
var risingdotseq = "#x2253";
var rlarr = "#x21C4";
var rlhar = "#x21CC";
var rmoust = "#x23B1";
var rmoustache = "#x23B1";
var rnmid = "#x2AEE";
var roang = "#x27ED";
var roarr = "#x21FE";
var robrk = "#x27E7";
var ropar = "#x2986";
var ropf = "#x1D563";
var roplus = "#x2A2E";
var rotimes = "#x2A35";
var rpargt = "#x2994";
var rppolint = "#x2A12";
var rrarr = "#x21C9";
var rscr = "#x1D4C7";
var rsh = "#x21B1";
var rsquor = "rsquo";
var rthree = "#x22CC";
var rtimes = "#x22CA";
var rtri = "#x25B9";
var rtrie = "#x22B5";
var rtrif = "#x25B8";
var rtriltri = "#x29CE";
var ruluhar = "#x2968";
var rx = "#x211E";
var sacute = "#x15B";
var sc = "#x227B";
var scE = "#x2AB4";
var scap = "#x2AB8";
var sccue = "#x227D";
var sce = "#x2AB0";
var scedil = "#x15F";
var scirc = "#x15D";
var scnE = "#x2AB6";
var scnap = "#x2ABA";
var scnsim = "#x22E9";
var scpolint = "#x2A13";
var scsim = "#x227F";
var scy = "#x441";
var sdotb = "#x22A1";
var sdote = "#x2A66";
var seArr = "#x21D8";
var searhk = "#x2925";
var searr = "#x2198";
var searrow = "#x2198";
var seswar = "#x2929";
var setminus = "#x2216";
var setmn = "#x2216";
var sext = "#x2736";
var sfr = "#x1D530";
var sfrown = "#x2322";
var sharp = "#x266F";
var shchcy = "#x449";
var shcy = "#x448";
var shortmid = "#x2223";
var shortparallel = "#x2225";
var sigmav = "sigmaf";
var simdot = "#x2A6A";
var sime = "#x2243";
var simeq = "#x2243";
var simg = "#x2A9E";
var simgE = "#x2AA0";
var siml = "#x2A9D";
var simlE = "#x2A9F";
var simne = "#x2246";
var simplus = "#x2A24";
var simrarr = "#x2972";
var slarr = "larr";
var smallsetminus = "#x2216";
var smashp = "#x2A33";
var smeparsl = "#x29E4";
var smid = "#x2223";
var smile = "#x2323";
var smt = "#x2AAA";
var smte = "#x2AAC";
var smtes = "#x2AAC;&#xFE00";
var softcy = "#x44C";
var solb = "#x29C4";
var solbar = "#x233F";
var sopf = "#x1D564";
var spadesuit = "spades";
var spar = "#x2225";
var sqcap = "#x2293";
var sqcaps = "#x2293;&#xFE00";
var sqcup = "#x2294";
var sqcups = "#x2294;&#xFE00";
var sqsub = "#x228F";
var sqsube = "#x2291";
var sqsubset = "#x228F";
var sqsubseteq = "#x2291";
var sqsup = "#x2290";
var sqsupe = "#x2292";
var sqsupset = "#x2290";
var sqsupseteq = "#x2292";
var squ = "#x25A1";
var square = "#x25A1";
var squarf = "#x25AA";
var squf = "#x25AA";
var srarr = "rarr";
var sscr = "#x1D4C8";
var ssetmn = "#x2216";
var ssmile = "#x2323";
var sstarf = "#x22C6";
var star = "#x2606";
var starf = "#x2605";
var straightepsilon = "#x3F5";
var straightphi = "#x3D5";
var strns = "macr";
var subE = "#x2AC5";
var subdot = "#x2ABD";
var subedot = "#x2AC3";
var submult = "#x2AC1";
var subnE = "#x2ACB";
var subne = "#x228A";
var subplus = "#x2ABF";
var subrarr = "#x2979";
var subset = "sub";
var subseteq = "sube";
var subseteqq = "#x2AC5";
var subsetneq = "#x228A";
var subsetneqq = "#x2ACB";
var subsim = "#x2AC7";
var subsub = "#x2AD5";
var subsup = "#x2AD3";
var succ = "#x227B";
var succapprox = "#x2AB8";
var succcurlyeq = "#x227D";
var succeq = "#x2AB0";
var succnapprox = "#x2ABA";
var succneqq = "#x2AB6";
var succnsim = "#x22E9";
var succsim = "#x227F";
var sung = "#x266A";
var supE = "#x2AC6";
var supdot = "#x2ABE";
var supdsub = "#x2AD8";
var supedot = "#x2AC4";
var suphsol = "#x27C9";
var suphsub = "#x2AD7";
var suplarr = "#x297B";
var supmult = "#x2AC2";
var supnE = "#x2ACC";
var supne = "#x228B";
var supplus = "#x2AC0";
var supset = "sup";
var supseteq = "supe";
var supseteqq = "#x2AC6";
var supsetneq = "#x228B";
var supsetneqq = "#x2ACC";
var supsim = "#x2AC8";
var supsub = "#x2AD4";
var supsup = "#x2AD6";
var swArr = "#x21D9";
var swarhk = "#x2926";
var swarr = "#x2199";
var swarrow = "#x2199";
var swnwar = "#x292A";
var target = "#x2316";
var tbrk = "#x23B4";
var tcaron = "#x165";
var tcedil = "#x163";
var tcy = "#x442";
var tdot = "#x20DB";
var telrec = "#x2315";
var tfr = "#x1D531";
var therefore = "there4";
var thetav = "#x3D1";
var thickapprox = "#x2248";
var thicksim = "sim";
var thkap = "#x2248";
var thksim = "sim";
var timesb = "#x22A0";
var timesbar = "#x2A31";
var timesd = "#x2A30";
var tint = "#x222D";
var toea = "#x2928";
var top = "#x22A4";
var topbot = "#x2336";
var topcir = "#x2AF1";
var topf = "#x1D565";
var topfork = "#x2ADA";
var tosa = "#x2929";
var tprime = "#x2034";
var triangle = "#x25B5";
var triangledown = "#x25BF";
var triangleleft = "#x25C3";
var trianglelefteq = "#x22B4";
var triangleq = "#x225C";
var triangleright = "#x25B9";
var trianglerighteq = "#x22B5";
var tridot = "#x25EC";
var trie = "#x225C";
var triminus = "#x2A3A";
var triplus = "#x2A39";
var trisb = "#x29CD";
var tritime = "#x2A3B";
var trpezium = "#x23E2";
var tscr = "#x1D4C9";
var tscy = "#x446";
var tshcy = "#x45B";
var tstrok = "#x167";
var twixt = "#x226C";
var twoheadleftarrow = "#x219E";
var twoheadrightarrow = "#x21A0";
var uHar = "#x2963";
var ubrcy = "#x45E";
var ubreve = "#x16D";
var ucy = "#x443";
var udarr = "#x21C5";
var udblac = "#x171";
var udhar = "#x296E";
var ufisht = "#x297E";
var ufr = "#x1D532";
var uharl = "#x21BF";
var uharr = "#x21BE";
var uhblk = "#x2580";
var ulcorn = "#x231C";
var ulcorner = "#x231C";
var ulcrop = "#x230F";
var ultri = "#x25F8";
var umacr = "#x16B";
var uogon = "#x173";
var uopf = "#x1D566";
var uparrow = "uarr";
var updownarrow = "#x2195";
var upharpoonleft = "#x21BF";
var upharpoonright = "#x21BE";
var uplus = "#x228E";
var upsi = "#x3C5";
var upuparrows = "#x21C8";
var urcorn = "#x231D";
var urcorner = "#x231D";
var urcrop = "#x230E";
var uring = "#x16F";
var urtri = "#x25F9";
var uscr = "#x1D4CA";
var utdot = "#x22F0";
var utilde = "#x169";
var utri = "#x25B5";
var utrif = "#x25B4";
var uuarr = "#x21C8";
var uwangle = "#x29A7";
var vArr = "#x21D5";
var vBar = "#x2AE8";
var vBarv = "#x2AE9";
var vDash = "#x22A8";
var vangrt = "#x299C";
var varepsilon = "#x3F5";
var varkappa = "#x3F0";
var varnothing = "empty";
var varphi = "#x3D5";
var varpi = "piv";
var varpropto = "prop";
var varr = "#x2195";
var varrho = "#x3F1";
var varsigma = "sigmaf";
var varsubsetneq = "#x228A;&#xFE00";
var varsubsetneqq = "#x2ACB;&#xFE00";
var varsupsetneq = "#x228B;&#xFE00";
var varsupsetneqq = "#x2ACC;&#xFE00";
var vartheta = "#x3D1";
var vartriangleleft = "#x22B2";
var vartriangleright = "#x22B3";
var vcy = "#x432";
var vdash = "#x22A2";
var vee = "or";
var veebar = "#x22BB";
var veeeq = "#x225A";
var vellip = "#x22EE";
var vfr = "#x1D533";
var vltri = "#x22B2";
var vnsub = "#x2282;&#x20D2";
var vnsup = "#x2283;&#x20D2";
var vopf = "#x1D567";
var vprop = "prop";
var vrtri = "#x22B3";
var vscr = "#x1D4CB";
var vsubnE = "#x2ACB;&#xFE00";
var vsubne = "#x228A;&#xFE00";
var vsupnE = "#x2ACC;&#xFE00";
var vsupne = "#x228B;&#xFE00";
var vzigzag = "#x299A";
var wcirc = "#x175";
var wedbar = "#x2A5F";
var wedge = "and";
var wedgeq = "#x2259";
var wfr = "#x1D534";
var wopf = "#x1D568";
var wp = "#x2118";
var wr = "#x2240";
var wreath = "#x2240";
var wscr = "#x1D4CC";
var xcap = "#x22C2";
var xcirc = "#x25EF";
var xcup = "#x22C3";
var xdtri = "#x25BD";
var xfr = "#x1D535";
var xhArr = "#x27FA";
var xharr = "#x27F7";
var xlArr = "#x27F8";
var xlarr = "#x27F5";
var xmap = "#x27FC";
var xnis = "#x22FB";
var xodot = "#x2A00";
var xopf = "#x1D569";
var xoplus = "#x2A01";
var xotime = "#x2A02";
var xrArr = "#x27F9";
var xrarr = "#x27F6";
var xscr = "#x1D4CD";
var xsqcup = "#x2A06";
var xuplus = "#x2A04";
var xutri = "#x25B3";
var xvee = "#x22C1";
var xwedge = "#x22C0";
var yacy = "#x44F";
var ycirc = "#x177";
var ycy = "#x44B";
var yfr = "#x1D536";
var yicy = "#x457";
var yopf = "#x1D56A";
var yscr = "#x1D4CE";
var yucy = "#x44E";
var zacute = "#x17A";
var zcaron = "#x17E";
var zcy = "#x437";
var zdot = "#x17C";
var zeetrf = "#x2128";
var zfr = "#x1D537";
var zhcy = "#x436";
var zigrarr = "#x21DD";
var zopf = "#x1D56B";
var zscr = "#x1D4CF";
var notEmailFriendly = {
	AMP: AMP,
	Abreve: Abreve,
	Acy: Acy,
	Afr: Afr,
	Amacr: Amacr,
	And: And,
	Aogon: Aogon,
	Aopf: Aopf,
	ApplyFunction: ApplyFunction,
	Ascr: Ascr,
	Assign: Assign,
	Backslash: Backslash,
	Barv: Barv,
	Barwed: Barwed,
	Bcy: Bcy,
	Because: Because,
	Bernoullis: Bernoullis,
	Bfr: Bfr,
	Bopf: Bopf,
	Breve: Breve,
	Bscr: Bscr,
	Bumpeq: Bumpeq,
	CHcy: CHcy,
	COPY: COPY,
	Cacute: Cacute,
	Cap: Cap,
	CapitalDifferentialD: CapitalDifferentialD,
	Cayleys: Cayleys,
	Ccaron: Ccaron,
	Ccirc: Ccirc,
	Cconint: Cconint,
	Cdot: Cdot,
	Cedilla: Cedilla,
	CenterDot: CenterDot,
	Cfr: Cfr,
	CircleDot: CircleDot,
	CircleMinus: CircleMinus,
	CirclePlus: CirclePlus,
	CircleTimes: CircleTimes,
	ClockwiseContourIntegral: ClockwiseContourIntegral,
	CloseCurlyDoubleQuote: CloseCurlyDoubleQuote,
	CloseCurlyQuote: CloseCurlyQuote,
	Colon: Colon,
	Colone: Colone,
	Congruent: Congruent,
	Conint: Conint,
	ContourIntegral: ContourIntegral,
	Copf: Copf,
	Coproduct: Coproduct,
	CounterClockwiseContourIntegral: CounterClockwiseContourIntegral,
	Cross: Cross,
	Cscr: Cscr,
	Cup: Cup,
	CupCap: CupCap,
	DD: DD,
	DDotrahd: DDotrahd,
	DJcy: DJcy,
	DScy: DScy,
	DZcy: DZcy,
	Darr: Darr,
	Dashv: Dashv,
	Dcaron: Dcaron,
	Dcy: Dcy,
	Del: Del,
	Dfr: Dfr,
	DiacriticalAcute: DiacriticalAcute,
	DiacriticalDot: DiacriticalDot,
	DiacriticalDoubleAcute: DiacriticalDoubleAcute,
	DiacriticalGrave: DiacriticalGrave,
	DiacriticalTilde: DiacriticalTilde,
	Diamond: Diamond,
	DifferentialD: DifferentialD,
	Dopf: Dopf,
	Dot: Dot,
	DotDot: DotDot,
	DotEqual: DotEqual,
	DoubleContourIntegral: DoubleContourIntegral,
	DoubleDot: DoubleDot,
	DoubleDownArrow: DoubleDownArrow,
	DoubleLeftArrow: DoubleLeftArrow,
	DoubleLeftRightArrow: DoubleLeftRightArrow,
	DoubleLeftTee: DoubleLeftTee,
	DoubleLongLeftArrow: DoubleLongLeftArrow,
	DoubleLongLeftRightArrow: DoubleLongLeftRightArrow,
	DoubleLongRightArrow: DoubleLongRightArrow,
	DoubleRightArrow: DoubleRightArrow,
	DoubleRightTee: DoubleRightTee,
	DoubleUpArrow: DoubleUpArrow,
	DoubleUpDownArrow: DoubleUpDownArrow,
	DoubleVerticalBar: DoubleVerticalBar,
	DownArrow: DownArrow,
	DownArrowBar: DownArrowBar,
	DownArrowUpArrow: DownArrowUpArrow,
	DownBreve: DownBreve,
	DownLeftRightVector: DownLeftRightVector,
	DownLeftTeeVector: DownLeftTeeVector,
	DownLeftVector: DownLeftVector,
	DownLeftVectorBar: DownLeftVectorBar,
	DownRightTeeVector: DownRightTeeVector,
	DownRightVector: DownRightVector,
	DownRightVectorBar: DownRightVectorBar,
	DownTee: DownTee,
	DownTeeArrow: DownTeeArrow,
	Downarrow: Downarrow,
	Dscr: Dscr,
	Dstrok: Dstrok,
	ENG: ENG,
	Ecaron: Ecaron,
	Ecy: Ecy,
	Edot: Edot,
	Efr: Efr,
	Element: Element,
	Emacr: Emacr,
	EmptySmallSquare: EmptySmallSquare,
	EmptyVerySmallSquare: EmptyVerySmallSquare,
	Eogon: Eogon,
	Eopf: Eopf,
	Equal: Equal,
	EqualTilde: EqualTilde,
	Equilibrium: Equilibrium,
	Escr: Escr,
	Esim: Esim,
	Exists: Exists,
	ExponentialE: ExponentialE,
	Fcy: Fcy,
	Ffr: Ffr,
	FilledSmallSquare: FilledSmallSquare,
	FilledVerySmallSquare: FilledVerySmallSquare,
	Fopf: Fopf,
	ForAll: ForAll,
	Fouriertrf: Fouriertrf,
	Fscr: Fscr,
	GJcy: GJcy,
	GT: GT,
	Gammad: Gammad,
	Gbreve: Gbreve,
	Gcedil: Gcedil,
	Gcirc: Gcirc,
	Gcy: Gcy,
	Gdot: Gdot,
	Gfr: Gfr,
	Gg: Gg,
	Gopf: Gopf,
	GreaterEqual: GreaterEqual,
	GreaterEqualLess: GreaterEqualLess,
	GreaterFullEqual: GreaterFullEqual,
	GreaterGreater: GreaterGreater,
	GreaterLess: GreaterLess,
	GreaterSlantEqual: GreaterSlantEqual,
	GreaterTilde: GreaterTilde,
	Gscr: Gscr,
	Gt: Gt,
	HARDcy: HARDcy,
	Hacek: Hacek,
	Hcirc: Hcirc,
	Hfr: Hfr,
	HilbertSpace: HilbertSpace,
	Hopf: Hopf,
	HorizontalLine: HorizontalLine,
	Hscr: Hscr,
	Hstrok: Hstrok,
	HumpDownHump: HumpDownHump,
	HumpEqual: HumpEqual,
	IEcy: IEcy,
	IJlig: IJlig,
	IOcy: IOcy,
	Icy: Icy,
	Idot: Idot,
	Ifr: Ifr,
	Im: Im,
	Imacr: Imacr,
	ImaginaryI: ImaginaryI,
	Implies: Implies,
	Int: Int,
	Integral: Integral,
	Intersection: Intersection,
	InvisibleComma: InvisibleComma,
	InvisibleTimes: InvisibleTimes,
	Iogon: Iogon,
	Iopf: Iopf,
	Iscr: Iscr,
	Itilde: Itilde,
	Iukcy: Iukcy,
	Jcirc: Jcirc,
	Jcy: Jcy,
	Jfr: Jfr,
	Jopf: Jopf,
	Jscr: Jscr,
	Jsercy: Jsercy,
	Jukcy: Jukcy,
	KHcy: KHcy,
	KJcy: KJcy,
	Kcedil: Kcedil,
	Kcy: Kcy,
	Kfr: Kfr,
	Kopf: Kopf,
	Kscr: Kscr,
	LJcy: LJcy,
	LT: LT,
	Lacute: Lacute,
	Lang: Lang,
	Laplacetrf: Laplacetrf,
	Larr: Larr,
	Lcaron: Lcaron,
	Lcedil: Lcedil,
	Lcy: Lcy,
	LeftAngleBracket: LeftAngleBracket,
	LeftArrow: LeftArrow,
	LeftArrowBar: LeftArrowBar,
	LeftArrowRightArrow: LeftArrowRightArrow,
	LeftCeiling: LeftCeiling,
	LeftDoubleBracket: LeftDoubleBracket,
	LeftDownTeeVector: LeftDownTeeVector,
	LeftDownVector: LeftDownVector,
	LeftDownVectorBar: LeftDownVectorBar,
	LeftFloor: LeftFloor,
	LeftRightArrow: LeftRightArrow,
	LeftRightVector: LeftRightVector,
	LeftTee: LeftTee,
	LeftTeeArrow: LeftTeeArrow,
	LeftTeeVector: LeftTeeVector,
	LeftTriangle: LeftTriangle,
	LeftTriangleBar: LeftTriangleBar,
	LeftTriangleEqual: LeftTriangleEqual,
	LeftUpDownVector: LeftUpDownVector,
	LeftUpTeeVector: LeftUpTeeVector,
	LeftUpVector: LeftUpVector,
	LeftUpVectorBar: LeftUpVectorBar,
	LeftVector: LeftVector,
	LeftVectorBar: LeftVectorBar,
	Leftarrow: Leftarrow,
	Leftrightarrow: Leftrightarrow,
	LessEqualGreater: LessEqualGreater,
	LessFullEqual: LessFullEqual,
	LessGreater: LessGreater,
	LessLess: LessLess,
	LessSlantEqual: LessSlantEqual,
	LessTilde: LessTilde,
	Lfr: Lfr,
	Ll: Ll,
	Lleftarrow: Lleftarrow,
	Lmidot: Lmidot,
	LongLeftArrow: LongLeftArrow,
	LongLeftRightArrow: LongLeftRightArrow,
	LongRightArrow: LongRightArrow,
	Longleftarrow: Longleftarrow,
	Longleftrightarrow: Longleftrightarrow,
	Longrightarrow: Longrightarrow,
	Lopf: Lopf,
	LowerLeftArrow: LowerLeftArrow,
	LowerRightArrow: LowerRightArrow,
	Lscr: Lscr,
	Lsh: Lsh,
	Lstrok: Lstrok,
	Lt: Lt,
	"Map": "#x2905",
	Mcy: Mcy,
	MediumSpace: MediumSpace,
	Mellintrf: Mellintrf,
	Mfr: Mfr,
	MinusPlus: MinusPlus,
	Mopf: Mopf,
	Mscr: Mscr,
	NJcy: NJcy,
	Nacute: Nacute,
	Ncaron: Ncaron,
	Ncedil: Ncedil,
	Ncy: Ncy,
	NegativeMediumSpace: NegativeMediumSpace,
	NegativeThickSpace: NegativeThickSpace,
	NegativeThinSpace: NegativeThinSpace,
	NegativeVeryThinSpace: NegativeVeryThinSpace,
	NestedGreaterGreater: NestedGreaterGreater,
	NestedLessLess: NestedLessLess,
	Nfr: Nfr,
	NoBreak: NoBreak,
	NonBreakingSpace: NonBreakingSpace,
	Nopf: Nopf,
	Not: Not,
	NotCongruent: NotCongruent,
	NotCupCap: NotCupCap,
	NotDoubleVerticalBar: NotDoubleVerticalBar,
	NotElement: NotElement,
	NotEqual: NotEqual,
	NotEqualTilde: NotEqualTilde,
	NotExists: NotExists,
	NotGreater: NotGreater,
	NotGreaterEqual: NotGreaterEqual,
	NotGreaterFullEqual: NotGreaterFullEqual,
	NotGreaterGreater: NotGreaterGreater,
	NotGreaterLess: NotGreaterLess,
	NotGreaterSlantEqual: NotGreaterSlantEqual,
	NotGreaterTilde: NotGreaterTilde,
	NotHumpDownHump: NotHumpDownHump,
	NotHumpEqual: NotHumpEqual,
	NotLeftTriangle: NotLeftTriangle,
	NotLeftTriangleBar: NotLeftTriangleBar,
	NotLeftTriangleEqual: NotLeftTriangleEqual,
	NotLess: NotLess,
	NotLessEqual: NotLessEqual,
	NotLessGreater: NotLessGreater,
	NotLessLess: NotLessLess,
	NotLessSlantEqual: NotLessSlantEqual,
	NotLessTilde: NotLessTilde,
	NotNestedGreaterGreater: NotNestedGreaterGreater,
	NotNestedLessLess: NotNestedLessLess,
	NotPrecedes: NotPrecedes,
	NotPrecedesEqual: NotPrecedesEqual,
	NotPrecedesSlantEqual: NotPrecedesSlantEqual,
	NotReverseElement: NotReverseElement,
	NotRightTriangle: NotRightTriangle,
	NotRightTriangleBar: NotRightTriangleBar,
	NotRightTriangleEqual: NotRightTriangleEqual,
	NotSquareSubset: NotSquareSubset,
	NotSquareSubsetEqual: NotSquareSubsetEqual,
	NotSquareSuperset: NotSquareSuperset,
	NotSquareSupersetEqual: NotSquareSupersetEqual,
	NotSubset: NotSubset,
	NotSubsetEqual: NotSubsetEqual,
	NotSucceeds: NotSucceeds,
	NotSucceedsEqual: NotSucceedsEqual,
	NotSucceedsSlantEqual: NotSucceedsSlantEqual,
	NotSucceedsTilde: NotSucceedsTilde,
	NotSuperset: NotSuperset,
	NotSupersetEqual: NotSupersetEqual,
	NotTilde: NotTilde,
	NotTildeEqual: NotTildeEqual,
	NotTildeFullEqual: NotTildeFullEqual,
	NotTildeTilde: NotTildeTilde,
	NotVerticalBar: NotVerticalBar,
	Nscr: Nscr,
	Ocy: Ocy,
	Odblac: Odblac,
	Ofr: Ofr,
	Omacr: Omacr,
	Oopf: Oopf,
	OpenCurlyDoubleQuote: OpenCurlyDoubleQuote,
	OpenCurlyQuote: OpenCurlyQuote,
	Or: Or,
	Oscr: Oscr,
	Otimes: Otimes,
	OverBar: OverBar,
	OverBrace: OverBrace,
	OverBracket: OverBracket,
	OverParenthesis: OverParenthesis,
	PartialD: PartialD,
	Pcy: Pcy,
	Pfr: Pfr,
	PlusMinus: PlusMinus,
	Poincareplane: Poincareplane,
	Popf: Popf,
	Pr: Pr,
	Precedes: Precedes,
	PrecedesEqual: PrecedesEqual,
	PrecedesSlantEqual: PrecedesSlantEqual,
	PrecedesTilde: PrecedesTilde,
	Product: Product,
	Proportion: Proportion,
	Proportional: Proportional,
	Pscr: Pscr,
	QUOT: QUOT,
	Qfr: Qfr,
	Qopf: Qopf,
	Qscr: Qscr,
	RBarr: RBarr,
	REG: REG,
	Racute: Racute,
	Rang: Rang,
	Rarr: Rarr,
	Rarrtl: Rarrtl,
	Rcaron: Rcaron,
	Rcedil: Rcedil,
	Rcy: Rcy,
	Re: Re,
	ReverseElement: ReverseElement,
	ReverseEquilibrium: ReverseEquilibrium,
	ReverseUpEquilibrium: ReverseUpEquilibrium,
	Rfr: Rfr,
	RightAngleBracket: RightAngleBracket,
	RightArrow: RightArrow,
	RightArrowBar: RightArrowBar,
	RightArrowLeftArrow: RightArrowLeftArrow,
	RightCeiling: RightCeiling,
	RightDoubleBracket: RightDoubleBracket,
	RightDownTeeVector: RightDownTeeVector,
	RightDownVector: RightDownVector,
	RightDownVectorBar: RightDownVectorBar,
	RightFloor: RightFloor,
	RightTee: RightTee,
	RightTeeArrow: RightTeeArrow,
	RightTeeVector: RightTeeVector,
	RightTriangle: RightTriangle,
	RightTriangleBar: RightTriangleBar,
	RightTriangleEqual: RightTriangleEqual,
	RightUpDownVector: RightUpDownVector,
	RightUpTeeVector: RightUpTeeVector,
	RightUpVector: RightUpVector,
	RightUpVectorBar: RightUpVectorBar,
	RightVector: RightVector,
	RightVectorBar: RightVectorBar,
	Rightarrow: Rightarrow,
	Ropf: Ropf,
	RoundImplies: RoundImplies,
	Rrightarrow: Rrightarrow,
	Rscr: Rscr,
	Rsh: Rsh,
	RuleDelayed: RuleDelayed,
	SHCHcy: SHCHcy,
	SHcy: SHcy,
	SOFTcy: SOFTcy,
	Sacute: Sacute,
	Sc: Sc,
	Scedil: Scedil,
	Scirc: Scirc,
	Scy: Scy,
	Sfr: Sfr,
	ShortDownArrow: ShortDownArrow,
	ShortLeftArrow: ShortLeftArrow,
	ShortRightArrow: ShortRightArrow,
	ShortUpArrow: ShortUpArrow,
	SmallCircle: SmallCircle,
	Sopf: Sopf,
	Sqrt: Sqrt,
	Square: Square,
	SquareIntersection: SquareIntersection,
	SquareSubset: SquareSubset,
	SquareSubsetEqual: SquareSubsetEqual,
	SquareSuperset: SquareSuperset,
	SquareSupersetEqual: SquareSupersetEqual,
	SquareUnion: SquareUnion,
	Sscr: Sscr,
	Star: Star,
	Sub: Sub,
	Subset: Subset,
	SubsetEqual: SubsetEqual,
	Succeeds: Succeeds,
	SucceedsEqual: SucceedsEqual,
	SucceedsSlantEqual: SucceedsSlantEqual,
	SucceedsTilde: SucceedsTilde,
	SuchThat: SuchThat,
	Sum: Sum,
	Sup: Sup,
	Superset: Superset,
	SupersetEqual: SupersetEqual,
	Supset: Supset,
	TRADE: TRADE,
	TSHcy: TSHcy,
	TScy: TScy,
	Tab: Tab,
	Tcaron: Tcaron,
	Tcedil: Tcedil,
	Tcy: Tcy,
	Tfr: Tfr,
	Therefore: Therefore,
	ThickSpace: ThickSpace,
	ThinSpace: ThinSpace,
	Tilde: Tilde,
	TildeEqual: TildeEqual,
	TildeFullEqual: TildeFullEqual,
	TildeTilde: TildeTilde,
	Topf: Topf,
	TripleDot: TripleDot,
	Tscr: Tscr,
	Tstrok: Tstrok,
	Uarr: Uarr,
	Uarrocir: Uarrocir,
	Ubrcy: Ubrcy,
	Ubreve: Ubreve,
	Ucy: Ucy,
	Udblac: Udblac,
	Ufr: Ufr,
	Umacr: Umacr,
	UnderBrace: UnderBrace,
	UnderBracket: UnderBracket,
	UnderParenthesis: UnderParenthesis,
	Union: Union,
	UnionPlus: UnionPlus,
	Uogon: Uogon,
	Uopf: Uopf,
	UpArrow: UpArrow,
	UpArrowBar: UpArrowBar,
	UpArrowDownArrow: UpArrowDownArrow,
	UpDownArrow: UpDownArrow,
	UpEquilibrium: UpEquilibrium,
	UpTee: UpTee,
	UpTeeArrow: UpTeeArrow,
	Uparrow: Uparrow,
	Updownarrow: Updownarrow,
	UpperLeftArrow: UpperLeftArrow,
	UpperRightArrow: UpperRightArrow,
	Upsi: Upsi,
	Uring: Uring,
	Uscr: Uscr,
	Utilde: Utilde,
	VDash: VDash,
	Vbar: Vbar,
	Vcy: Vcy,
	Vdash: Vdash,
	Vdashl: Vdashl,
	Vee: Vee,
	Verbar: Verbar,
	Vert: Vert,
	VerticalBar: VerticalBar,
	VerticalSeparator: VerticalSeparator,
	VerticalTilde: VerticalTilde,
	VeryThinSpace: VeryThinSpace,
	Vfr: Vfr,
	Vopf: Vopf,
	Vscr: Vscr,
	Vvdash: Vvdash,
	Wcirc: Wcirc,
	Wedge: Wedge,
	Wfr: Wfr,
	Wopf: Wopf,
	Wscr: Wscr,
	Xfr: Xfr,
	Xopf: Xopf,
	Xscr: Xscr,
	YAcy: YAcy,
	YIcy: YIcy,
	YUcy: YUcy,
	Ycirc: Ycirc,
	Ycy: Ycy,
	Yfr: Yfr,
	Yopf: Yopf,
	Yscr: Yscr,
	ZHcy: ZHcy,
	Zacute: Zacute,
	Zcaron: Zcaron,
	Zcy: Zcy,
	Zdot: Zdot,
	ZeroWidthSpace: ZeroWidthSpace,
	Zfr: Zfr,
	Zopf: Zopf,
	Zscr: Zscr,
	abreve: abreve,
	ac: ac,
	acE: acE,
	acd: acd,
	acy: acy,
	af: af,
	afr: afr,
	aleph: aleph,
	amacr: amacr,
	amalg: amalg,
	andand: andand,
	andd: andd,
	andslope: andslope,
	andv: andv,
	ange: ange,
	angle: angle,
	angmsd: angmsd,
	angmsdaa: angmsdaa,
	angmsdab: angmsdab,
	angmsdac: angmsdac,
	angmsdad: angmsdad,
	angmsdae: angmsdae,
	angmsdaf: angmsdaf,
	angmsdag: angmsdag,
	angmsdah: angmsdah,
	angrt: angrt,
	angrtvb: angrtvb,
	angrtvbd: angrtvbd,
	angsph: angsph,
	angst: angst,
	angzarr: angzarr,
	aogon: aogon,
	aopf: aopf,
	ap: ap,
	apE: apE,
	apacir: apacir,
	ape: ape,
	apid: apid,
	approx: approx,
	approxeq: approxeq,
	ascr: ascr,
	asympeq: asympeq,
	awconint: awconint,
	awint: awint,
	bNot: bNot,
	backcong: backcong,
	backepsilon: backepsilon,
	backprime: backprime,
	backsim: backsim,
	backsimeq: backsimeq,
	barvee: barvee,
	barwed: barwed,
	barwedge: barwedge,
	bbrk: bbrk,
	bbrktbrk: bbrktbrk,
	bcong: bcong,
	bcy: bcy,
	becaus: becaus,
	because: because,
	bemptyv: bemptyv,
	bepsi: bepsi,
	bernou: bernou,
	beth: beth,
	between: between,
	bfr: bfr,
	bigcap: bigcap,
	bigcirc: bigcirc,
	bigcup: bigcup,
	bigodot: bigodot,
	bigoplus: bigoplus,
	bigotimes: bigotimes,
	bigsqcup: bigsqcup,
	bigstar: bigstar,
	bigtriangledown: bigtriangledown,
	bigtriangleup: bigtriangleup,
	biguplus: biguplus,
	bigvee: bigvee,
	bigwedge: bigwedge,
	bkarow: bkarow,
	blacklozenge: blacklozenge,
	blacksquare: blacksquare,
	blacktriangle: blacktriangle,
	blacktriangledown: blacktriangledown,
	blacktriangleleft: blacktriangleleft,
	blacktriangleright: blacktriangleright,
	blank: blank,
	blk12: blk12,
	blk14: blk14,
	blk34: blk34,
	block: block,
	bne: bne,
	bnequiv: bnequiv,
	bnot: bnot,
	bopf: bopf,
	bot: bot,
	bottom: bottom,
	bowtie: bowtie,
	boxDL: boxDL,
	boxDR: boxDR,
	boxDl: boxDl,
	boxDr: boxDr,
	boxH: boxH,
	boxHD: boxHD,
	boxHU: boxHU,
	boxHd: boxHd,
	boxHu: boxHu,
	boxUL: boxUL,
	boxUR: boxUR,
	boxUl: boxUl,
	boxUr: boxUr,
	boxV: boxV,
	boxVH: boxVH,
	boxVL: boxVL,
	boxVR: boxVR,
	boxVh: boxVh,
	boxVl: boxVl,
	boxVr: boxVr,
	boxbox: boxbox,
	boxdL: boxdL,
	boxdR: boxdR,
	boxdl: boxdl,
	boxdr: boxdr,
	boxh: boxh,
	boxhD: boxhD,
	boxhU: boxhU,
	boxhd: boxhd,
	boxhu: boxhu,
	boxminus: boxminus,
	boxplus: boxplus,
	boxtimes: boxtimes,
	boxuL: boxuL,
	boxuR: boxuR,
	boxul: boxul,
	boxur: boxur,
	boxv: boxv,
	boxvH: boxvH,
	boxvL: boxvL,
	boxvR: boxvR,
	boxvh: boxvh,
	boxvl: boxvl,
	boxvr: boxvr,
	bprime: bprime,
	breve: breve,
	bscr: bscr,
	bsemi: bsemi,
	bsim: bsim,
	bsime: bsime,
	bsolb: bsolb,
	bsolhsub: bsolhsub,
	bullet: bullet,
	bump: bump,
	bumpE: bumpE,
	bumpe: bumpe,
	bumpeq: bumpeq,
	cacute: cacute,
	capand: capand,
	capbrcup: capbrcup,
	capcap: capcap,
	capcup: capcup,
	capdot: capdot,
	caps: caps,
	caret: caret,
	caron: caron,
	ccaps: ccaps,
	ccaron: ccaron,
	ccirc: ccirc,
	ccups: ccups,
	ccupssm: ccupssm,
	cdot: cdot,
	cemptyv: cemptyv,
	centerdot: centerdot,
	cfr: cfr,
	chcy: chcy,
	check: check,
	checkmark: checkmark,
	cir: cir,
	cirE: cirE,
	circeq: circeq,
	circlearrowleft: circlearrowleft,
	circlearrowright: circlearrowright,
	circledR: circledR,
	circledS: circledS,
	circledast: circledast,
	circledcirc: circledcirc,
	circleddash: circleddash,
	cire: cire,
	cirfnint: cirfnint,
	cirmid: cirmid,
	cirscir: cirscir,
	clubsuit: clubsuit,
	colone: colone,
	coloneq: coloneq,
	comp: comp,
	compfn: compfn,
	complement: complement,
	complexes: complexes,
	congdot: congdot,
	conint: conint,
	copf: copf,
	coprod: coprod,
	copysr: copysr,
	cross: cross,
	cscr: cscr,
	csub: csub,
	csube: csube,
	csup: csup,
	csupe: csupe,
	ctdot: ctdot,
	cudarrl: cudarrl,
	cudarrr: cudarrr,
	cuepr: cuepr,
	cuesc: cuesc,
	cularr: cularr,
	cularrp: cularrp,
	cupbrcap: cupbrcap,
	cupcap: cupcap,
	cupcup: cupcup,
	cupdot: cupdot,
	cupor: cupor,
	cups: cups,
	curarr: curarr,
	curarrm: curarrm,
	curlyeqprec: curlyeqprec,
	curlyeqsucc: curlyeqsucc,
	curlyvee: curlyvee,
	curlywedge: curlywedge,
	curvearrowleft: curvearrowleft,
	curvearrowright: curvearrowright,
	cuvee: cuvee,
	cuwed: cuwed,
	cwconint: cwconint,
	cwint: cwint,
	cylcty: cylcty,
	dHar: dHar,
	daleth: daleth,
	dash: dash,
	dashv: dashv,
	dbkarow: dbkarow,
	dblac: dblac,
	dcaron: dcaron,
	dcy: dcy,
	dd: dd,
	ddagger: ddagger,
	ddarr: ddarr,
	ddotseq: ddotseq,
	demptyv: demptyv,
	dfisht: dfisht,
	dfr: dfr,
	dharl: dharl,
	dharr: dharr,
	diam: diam,
	diamond: diamond,
	diamondsuit: diamondsuit,
	die: die,
	digamma: digamma,
	disin: disin,
	div: div,
	divideontimes: divideontimes,
	divonx: divonx,
	djcy: djcy,
	dlcorn: dlcorn,
	dlcrop: dlcrop,
	dopf: dopf,
	dot: dot,
	doteq: doteq,
	doteqdot: doteqdot,
	dotminus: dotminus,
	dotplus: dotplus,
	dotsquare: dotsquare,
	doublebarwedge: doublebarwedge,
	downarrow: downarrow,
	downdownarrows: downdownarrows,
	downharpoonleft: downharpoonleft,
	downharpoonright: downharpoonright,
	drbkarow: drbkarow,
	drcorn: drcorn,
	drcrop: drcrop,
	dscr: dscr,
	dscy: dscy,
	dsol: dsol,
	dstrok: dstrok,
	dtdot: dtdot,
	dtri: dtri,
	dtrif: dtrif,
	duarr: duarr,
	duhar: duhar,
	dwangle: dwangle,
	dzcy: dzcy,
	dzigrarr: dzigrarr,
	eDDot: eDDot,
	eDot: eDot,
	easter: easter,
	ecaron: ecaron,
	ecir: ecir,
	ecolon: ecolon,
	ecy: ecy,
	edot: edot,
	ee: ee,
	efDot: efDot,
	efr: efr,
	eg: eg,
	egs: egs,
	egsdot: egsdot,
	el: el,
	elinters: elinters,
	ell: ell,
	els: els,
	elsdot: elsdot,
	emacr: emacr,
	emptyset: emptyset,
	emptyv: emptyv,
	emsp13: emsp13,
	emsp14: emsp14,
	eng: eng,
	eogon: eogon,
	eopf: eopf,
	epar: epar,
	eparsl: eparsl,
	eplus: eplus,
	epsi: epsi,
	epsiv: epsiv,
	eqcirc: eqcirc,
	eqcolon: eqcolon,
	eqsim: eqsim,
	eqslantgtr: eqslantgtr,
	eqslantless: eqslantless,
	equest: equest,
	equivDD: equivDD,
	eqvparsl: eqvparsl,
	erDot: erDot,
	erarr: erarr,
	escr: escr,
	esdot: esdot,
	esim: esim,
	expectation: expectation,
	exponentiale: exponentiale,
	fallingdotseq: fallingdotseq,
	fcy: fcy,
	female: female,
	ffilig: ffilig,
	fflig: fflig,
	ffllig: ffllig,
	ffr: ffr,
	filig: filig,
	flat: flat,
	fllig: fllig,
	fltns: fltns,
	fopf: fopf,
	fork: fork,
	forkv: forkv,
	fpartint: fpartint,
	frac13: frac13,
	frac15: frac15,
	frac16: frac16,
	frac18: frac18,
	frac23: frac23,
	frac25: frac25,
	frac35: frac35,
	frac38: frac38,
	frac45: frac45,
	frac56: frac56,
	frac58: frac58,
	frac78: frac78,
	frown: frown,
	fscr: fscr,
	gE: gE,
	gEl: gEl,
	gacute: gacute,
	gammad: gammad,
	gap: gap,
	gbreve: gbreve,
	gcirc: gcirc,
	gcy: gcy,
	gdot: gdot,
	gel: gel,
	geq: geq,
	geqq: geqq,
	geqslant: geqslant,
	ges: ges,
	gescc: gescc,
	gesdot: gesdot,
	gesdoto: gesdoto,
	gesdotol: gesdotol,
	gesl: gesl,
	gesles: gesles,
	gfr: gfr,
	gg: gg,
	ggg: ggg,
	gimel: gimel,
	gjcy: gjcy,
	gl: gl,
	glE: glE,
	gla: gla,
	glj: glj,
	gnE: gnE,
	gnap: gnap,
	gnapprox: gnapprox,
	gne: gne,
	gneq: gneq,
	gneqq: gneqq,
	gnsim: gnsim,
	gopf: gopf,
	grave: grave,
	gscr: gscr,
	gsim: gsim,
	gsime: gsime,
	gsiml: gsiml,
	gtcc: gtcc,
	gtcir: gtcir,
	gtdot: gtdot,
	gtlPar: gtlPar,
	gtquest: gtquest,
	gtrapprox: gtrapprox,
	gtrarr: gtrarr,
	gtrdot: gtrdot,
	gtreqless: gtreqless,
	gtreqqless: gtreqqless,
	gtrless: gtrless,
	gtrsim: gtrsim,
	gvertneqq: gvertneqq,
	gvnE: gvnE,
	hairsp: hairsp,
	half: half,
	hamilt: hamilt,
	hardcy: hardcy,
	harrcir: harrcir,
	harrw: harrw,
	hbar: hbar,
	hcirc: hcirc,
	heartsuit: heartsuit,
	hercon: hercon,
	hfr: hfr,
	hksearow: hksearow,
	hkswarow: hkswarow,
	hoarr: hoarr,
	homtht: homtht,
	hookleftarrow: hookleftarrow,
	hookrightarrow: hookrightarrow,
	hopf: hopf,
	horbar: horbar,
	hscr: hscr,
	hslash: hslash,
	hstrok: hstrok,
	hybull: hybull,
	hyphen: hyphen,
	ic: ic,
	icy: icy,
	iecy: iecy,
	iff: iff,
	ifr: ifr,
	ii: ii,
	iiiint: iiiint,
	iiint: iiint,
	iinfin: iinfin,
	iiota: iiota,
	ijlig: ijlig,
	imacr: imacr,
	imagline: imagline,
	imagpart: imagpart,
	imath: imath,
	imof: imof,
	imped: imped,
	"in": "#x2208",
	incare: incare,
	infintie: infintie,
	inodot: inodot,
	intcal: intcal,
	integers: integers,
	intercal: intercal,
	intlarhk: intlarhk,
	intprod: intprod,
	iocy: iocy,
	iogon: iogon,
	iopf: iopf,
	iprod: iprod,
	iscr: iscr,
	isinE: isinE,
	isindot: isindot,
	isins: isins,
	isinsv: isinsv,
	isinv: isinv,
	it: it,
	itilde: itilde,
	iukcy: iukcy,
	jcirc: jcirc,
	jcy: jcy,
	jfr: jfr,
	jmath: jmath,
	jopf: jopf,
	jscr: jscr,
	jsercy: jsercy,
	jukcy: jukcy,
	kappav: kappav,
	kcedil: kcedil,
	kcy: kcy,
	kfr: kfr,
	kgreen: kgreen,
	khcy: khcy,
	kjcy: kjcy,
	kopf: kopf,
	kscr: kscr,
	lAarr: lAarr,
	lAtail: lAtail,
	lBarr: lBarr,
	lE: lE,
	lEg: lEg,
	lHar: lHar,
	lacute: lacute,
	laemptyv: laemptyv,
	lagran: lagran,
	langd: langd,
	langle: langle,
	lap: lap,
	larrb: larrb,
	larrbfs: larrbfs,
	larrfs: larrfs,
	larrhk: larrhk,
	larrlp: larrlp,
	larrpl: larrpl,
	larrsim: larrsim,
	larrtl: larrtl,
	lat: lat,
	latail: latail,
	late: late,
	lates: lates,
	lbarr: lbarr,
	lbbrk: lbbrk,
	lbrace: lbrace,
	lbrack: lbrack,
	lbrke: lbrke,
	lbrksld: lbrksld,
	lbrkslu: lbrkslu,
	lcaron: lcaron,
	lcedil: lcedil,
	lcub: lcub,
	lcy: lcy,
	ldca: ldca,
	ldquor: ldquor,
	ldrdhar: ldrdhar,
	ldrushar: ldrushar,
	ldsh: ldsh,
	leftarrow: leftarrow,
	leftarrowtail: leftarrowtail,
	leftharpoondown: leftharpoondown,
	leftharpoonup: leftharpoonup,
	leftleftarrows: leftleftarrows,
	leftrightarrow: leftrightarrow,
	leftrightarrows: leftrightarrows,
	leftrightharpoons: leftrightharpoons,
	leftrightsquigarrow: leftrightsquigarrow,
	leftthreetimes: leftthreetimes,
	leg: leg,
	leq: leq,
	leqq: leqq,
	leqslant: leqslant,
	les: les,
	lescc: lescc,
	lesdot: lesdot,
	lesdoto: lesdoto,
	lesdotor: lesdotor,
	lesg: lesg,
	lesges: lesges,
	lessapprox: lessapprox,
	lessdot: lessdot,
	lesseqgtr: lesseqgtr,
	lesseqqgtr: lesseqqgtr,
	lessgtr: lessgtr,
	lesssim: lesssim,
	lfisht: lfisht,
	lfr: lfr,
	lg: lg,
	lgE: lgE,
	lhard: lhard,
	lharu: lharu,
	lharul: lharul,
	lhblk: lhblk,
	ljcy: ljcy,
	ll: ll,
	llarr: llarr,
	llcorner: llcorner,
	llhard: llhard,
	lltri: lltri,
	lmidot: lmidot,
	lmoust: lmoust,
	lmoustache: lmoustache,
	lnE: lnE,
	lnap: lnap,
	lnapprox: lnapprox,
	lne: lne,
	lneq: lneq,
	lneqq: lneqq,
	lnsim: lnsim,
	loang: loang,
	loarr: loarr,
	lobrk: lobrk,
	longleftarrow: longleftarrow,
	longleftrightarrow: longleftrightarrow,
	longmapsto: longmapsto,
	longrightarrow: longrightarrow,
	looparrowleft: looparrowleft,
	looparrowright: looparrowright,
	lopar: lopar,
	lopf: lopf,
	loplus: loplus,
	lotimes: lotimes,
	lozenge: lozenge,
	lozf: lozf,
	lparlt: lparlt,
	lrarr: lrarr,
	lrcorner: lrcorner,
	lrhar: lrhar,
	lrhard: lrhard,
	lrtri: lrtri,
	lscr: lscr,
	lsh: lsh,
	lsim: lsim,
	lsime: lsime,
	lsimg: lsimg,
	lsquor: lsquor,
	lstrok: lstrok,
	ltcc: ltcc,
	ltcir: ltcir,
	ltdot: ltdot,
	lthree: lthree,
	ltimes: ltimes,
	ltlarr: ltlarr,
	ltquest: ltquest,
	ltrPar: ltrPar,
	ltri: ltri,
	ltrie: ltrie,
	ltrif: ltrif,
	lurdshar: lurdshar,
	luruhar: luruhar,
	lvertneqq: lvertneqq,
	lvnE: lvnE,
	mDDot: mDDot,
	male: male,
	malt: malt,
	maltese: maltese,
	map: map,
	mapsto: mapsto,
	mapstodown: mapstodown,
	mapstoleft: mapstoleft,
	mapstoup: mapstoup,
	marker: marker,
	mcomma: mcomma,
	mcy: mcy,
	measuredangle: measuredangle,
	mfr: mfr,
	mho: mho,
	mid: mid,
	midcir: midcir,
	minusb: minusb,
	minusd: minusd,
	minusdu: minusdu,
	mlcp: mlcp,
	mldr: mldr,
	mnplus: mnplus,
	models: models,
	mopf: mopf,
	mp: mp,
	mscr: mscr,
	mstpos: mstpos,
	multimap: multimap,
	mumap: mumap,
	nGg: nGg,
	nGt: nGt,
	nGtv: nGtv,
	nLeftarrow: nLeftarrow,
	nLeftrightarrow: nLeftrightarrow,
	nLl: nLl,
	nLt: nLt,
	nLtv: nLtv,
	nRightarrow: nRightarrow,
	nVDash: nVDash,
	nVdash: nVdash,
	nacute: nacute,
	nang: nang,
	nap: nap,
	napE: napE,
	napid: napid,
	napos: napos,
	napprox: napprox,
	natur: natur,
	natural: natural,
	naturals: naturals,
	nbump: nbump,
	nbumpe: nbumpe,
	ncap: ncap,
	ncaron: ncaron,
	ncedil: ncedil,
	ncong: ncong,
	ncongdot: ncongdot,
	ncup: ncup,
	ncy: ncy,
	neArr: neArr,
	nearhk: nearhk,
	nearr: nearr,
	nearrow: nearrow,
	nedot: nedot,
	nequiv: nequiv,
	nesear: nesear,
	nesim: nesim,
	nexist: nexist,
	nexists: nexists,
	nfr: nfr,
	ngE: ngE,
	nge: nge,
	ngeq: ngeq,
	ngeqq: ngeqq,
	ngeqslant: ngeqslant,
	nges: nges,
	ngsim: ngsim,
	ngt: ngt,
	ngtr: ngtr,
	nhArr: nhArr,
	nharr: nharr,
	nhpar: nhpar,
	nis: nis,
	nisd: nisd,
	niv: niv,
	njcy: njcy,
	nlArr: nlArr,
	nlE: nlE,
	nlarr: nlarr,
	nldr: nldr,
	nle: nle,
	nleftarrow: nleftarrow,
	nleftrightarrow: nleftrightarrow,
	nleq: nleq,
	nleqq: nleqq,
	nleqslant: nleqslant,
	nles: nles,
	nless: nless,
	nlsim: nlsim,
	nlt: nlt,
	nltri: nltri,
	nltrie: nltrie,
	nmid: nmid,
	nopf: nopf,
	notinE: notinE,
	notindot: notindot,
	notinva: notinva,
	notinvb: notinvb,
	notinvc: notinvc,
	notni: notni,
	notniva: notniva,
	notnivb: notnivb,
	notnivc: notnivc,
	npar: npar,
	nparallel: nparallel,
	nparsl: nparsl,
	npart: npart,
	npolint: npolint,
	npr: npr,
	nprcue: nprcue,
	npre: npre,
	nprec: nprec,
	npreceq: npreceq,
	nrArr: nrArr,
	nrarr: nrarr,
	nrarrc: nrarrc,
	nrarrw: nrarrw,
	nrightarrow: nrightarrow,
	nrtri: nrtri,
	nrtrie: nrtrie,
	nsc: nsc,
	nsccue: nsccue,
	nsce: nsce,
	nscr: nscr,
	nshortmid: nshortmid,
	nshortparallel: nshortparallel,
	nsim: nsim,
	nsime: nsime,
	nsimeq: nsimeq,
	nsmid: nsmid,
	nspar: nspar,
	nsqsube: nsqsube,
	nsqsupe: nsqsupe,
	nsubE: nsubE,
	nsube: nsube,
	nsubset: nsubset,
	nsubseteq: nsubseteq,
	nsubseteqq: nsubseteqq,
	nsucc: nsucc,
	nsucceq: nsucceq,
	nsup: nsup,
	nsupE: nsupE,
	nsupe: nsupe,
	nsupset: nsupset,
	nsupseteq: nsupseteq,
	nsupseteqq: nsupseteqq,
	ntgl: ntgl,
	ntlg: ntlg,
	ntriangleleft: ntriangleleft,
	ntrianglelefteq: ntrianglelefteq,
	ntriangleright: ntriangleright,
	ntrianglerighteq: ntrianglerighteq,
	numero: numero,
	numsp: numsp,
	nvDash: nvDash,
	nvHarr: nvHarr,
	nvap: nvap,
	nvdash: nvdash,
	nvge: nvge,
	nvgt: nvgt,
	nvinfin: nvinfin,
	nvlArr: nvlArr,
	nvle: nvle,
	nvlt: nvlt,
	nvltrie: nvltrie,
	nvrArr: nvrArr,
	nvrtrie: nvrtrie,
	nvsim: nvsim,
	nwArr: nwArr,
	nwarhk: nwarhk,
	nwarr: nwarr,
	nwarrow: nwarrow,
	nwnear: nwnear,
	oS: oS,
	oast: oast,
	ocir: ocir,
	ocy: ocy,
	odash: odash,
	odblac: odblac,
	odiv: odiv,
	odot: odot,
	odsold: odsold,
	ofcir: ofcir,
	ofr: ofr,
	ogon: ogon,
	ogt: ogt,
	ohbar: ohbar,
	ohm: ohm,
	oint: oint,
	olarr: olarr,
	olcir: olcir,
	olcross: olcross,
	olt: olt,
	omacr: omacr,
	omid: omid,
	ominus: ominus,
	oopf: oopf,
	opar: opar,
	operp: operp,
	orarr: orarr,
	ord: ord,
	order: order,
	orderof: orderof,
	origof: origof,
	oror: oror,
	orslope: orslope,
	orv: orv,
	oscr: oscr,
	osol: osol,
	otimesas: otimesas,
	ovbar: ovbar,
	par: par,
	parallel: parallel,
	parsim: parsim,
	parsl: parsl,
	pcy: pcy,
	pertenk: pertenk,
	pfr: pfr,
	phiv: phiv,
	phmmat: phmmat,
	phone: phone,
	pitchfork: pitchfork,
	planck: planck,
	planckh: planckh,
	plankv: plankv,
	plusacir: plusacir,
	plusb: plusb,
	pluscir: pluscir,
	plusdo: plusdo,
	plusdu: plusdu,
	pluse: pluse,
	plussim: plussim,
	plustwo: plustwo,
	pm: pm,
	pointint: pointint,
	popf: popf,
	pr: pr,
	prE: prE,
	prap: prap,
	prcue: prcue,
	pre: pre,
	prec: prec,
	precapprox: precapprox,
	preccurlyeq: preccurlyeq,
	preceq: preceq,
	precnapprox: precnapprox,
	precneqq: precneqq,
	precnsim: precnsim,
	precsim: precsim,
	primes: primes,
	prnE: prnE,
	prnap: prnap,
	prnsim: prnsim,
	profalar: profalar,
	profline: profline,
	profsurf: profsurf,
	propto: propto,
	prsim: prsim,
	prurel: prurel,
	pscr: pscr,
	puncsp: puncsp,
	qfr: qfr,
	qint: qint,
	qopf: qopf,
	qprime: qprime,
	qscr: qscr,
	quaternions: quaternions,
	quatint: quatint,
	questeq: questeq,
	rAarr: rAarr,
	rAtail: rAtail,
	rBarr: rBarr,
	rHar: rHar,
	race: race,
	racute: racute,
	raemptyv: raemptyv,
	rangd: rangd,
	range: range,
	rangle: rangle,
	rarrap: rarrap,
	rarrb: rarrb,
	rarrbfs: rarrbfs,
	rarrc: rarrc,
	rarrfs: rarrfs,
	rarrhk: rarrhk,
	rarrlp: rarrlp,
	rarrpl: rarrpl,
	rarrsim: rarrsim,
	rarrtl: rarrtl,
	rarrw: rarrw,
	ratail: ratail,
	ratio: ratio,
	rationals: rationals,
	rbarr: rbarr,
	rbbrk: rbbrk,
	rbrke: rbrke,
	rbrksld: rbrksld,
	rbrkslu: rbrkslu,
	rcaron: rcaron,
	rcedil: rcedil,
	rcy: rcy,
	rdca: rdca,
	rdldhar: rdldhar,
	rdquor: rdquor,
	rdsh: rdsh,
	realine: realine,
	realpart: realpart,
	reals: reals,
	rect: rect,
	rfisht: rfisht,
	rfr: rfr,
	rhard: rhard,
	rharu: rharu,
	rharul: rharul,
	rhov: rhov,
	rightarrow: rightarrow,
	rightarrowtail: rightarrowtail,
	rightharpoondown: rightharpoondown,
	rightharpoonup: rightharpoonup,
	rightleftarrows: rightleftarrows,
	rightleftharpoons: rightleftharpoons,
	rightrightarrows: rightrightarrows,
	rightsquigarrow: rightsquigarrow,
	rightthreetimes: rightthreetimes,
	ring: ring,
	risingdotseq: risingdotseq,
	rlarr: rlarr,
	rlhar: rlhar,
	rmoust: rmoust,
	rmoustache: rmoustache,
	rnmid: rnmid,
	roang: roang,
	roarr: roarr,
	robrk: robrk,
	ropar: ropar,
	ropf: ropf,
	roplus: roplus,
	rotimes: rotimes,
	rpargt: rpargt,
	rppolint: rppolint,
	rrarr: rrarr,
	rscr: rscr,
	rsh: rsh,
	rsquor: rsquor,
	rthree: rthree,
	rtimes: rtimes,
	rtri: rtri,
	rtrie: rtrie,
	rtrif: rtrif,
	rtriltri: rtriltri,
	ruluhar: ruluhar,
	rx: rx,
	sacute: sacute,
	sc: sc,
	scE: scE,
	scap: scap,
	sccue: sccue,
	sce: sce,
	scedil: scedil,
	scirc: scirc,
	scnE: scnE,
	scnap: scnap,
	scnsim: scnsim,
	scpolint: scpolint,
	scsim: scsim,
	scy: scy,
	sdotb: sdotb,
	sdote: sdote,
	seArr: seArr,
	searhk: searhk,
	searr: searr,
	searrow: searrow,
	seswar: seswar,
	setminus: setminus,
	setmn: setmn,
	sext: sext,
	sfr: sfr,
	sfrown: sfrown,
	sharp: sharp,
	shchcy: shchcy,
	shcy: shcy,
	shortmid: shortmid,
	shortparallel: shortparallel,
	sigmav: sigmav,
	simdot: simdot,
	sime: sime,
	simeq: simeq,
	simg: simg,
	simgE: simgE,
	siml: siml,
	simlE: simlE,
	simne: simne,
	simplus: simplus,
	simrarr: simrarr,
	slarr: slarr,
	smallsetminus: smallsetminus,
	smashp: smashp,
	smeparsl: smeparsl,
	smid: smid,
	smile: smile,
	smt: smt,
	smte: smte,
	smtes: smtes,
	softcy: softcy,
	solb: solb,
	solbar: solbar,
	sopf: sopf,
	spadesuit: spadesuit,
	spar: spar,
	sqcap: sqcap,
	sqcaps: sqcaps,
	sqcup: sqcup,
	sqcups: sqcups,
	sqsub: sqsub,
	sqsube: sqsube,
	sqsubset: sqsubset,
	sqsubseteq: sqsubseteq,
	sqsup: sqsup,
	sqsupe: sqsupe,
	sqsupset: sqsupset,
	sqsupseteq: sqsupseteq,
	squ: squ,
	square: square,
	squarf: squarf,
	squf: squf,
	srarr: srarr,
	sscr: sscr,
	ssetmn: ssetmn,
	ssmile: ssmile,
	sstarf: sstarf,
	star: star,
	starf: starf,
	straightepsilon: straightepsilon,
	straightphi: straightphi,
	strns: strns,
	subE: subE,
	subdot: subdot,
	subedot: subedot,
	submult: submult,
	subnE: subnE,
	subne: subne,
	subplus: subplus,
	subrarr: subrarr,
	subset: subset,
	subseteq: subseteq,
	subseteqq: subseteqq,
	subsetneq: subsetneq,
	subsetneqq: subsetneqq,
	subsim: subsim,
	subsub: subsub,
	subsup: subsup,
	succ: succ,
	succapprox: succapprox,
	succcurlyeq: succcurlyeq,
	succeq: succeq,
	succnapprox: succnapprox,
	succneqq: succneqq,
	succnsim: succnsim,
	succsim: succsim,
	sung: sung,
	supE: supE,
	supdot: supdot,
	supdsub: supdsub,
	supedot: supedot,
	suphsol: suphsol,
	suphsub: suphsub,
	suplarr: suplarr,
	supmult: supmult,
	supnE: supnE,
	supne: supne,
	supplus: supplus,
	supset: supset,
	supseteq: supseteq,
	supseteqq: supseteqq,
	supsetneq: supsetneq,
	supsetneqq: supsetneqq,
	supsim: supsim,
	supsub: supsub,
	supsup: supsup,
	swArr: swArr,
	swarhk: swarhk,
	swarr: swarr,
	swarrow: swarrow,
	swnwar: swnwar,
	target: target,
	tbrk: tbrk,
	tcaron: tcaron,
	tcedil: tcedil,
	tcy: tcy,
	tdot: tdot,
	telrec: telrec,
	tfr: tfr,
	therefore: therefore,
	thetav: thetav,
	thickapprox: thickapprox,
	thicksim: thicksim,
	thkap: thkap,
	thksim: thksim,
	timesb: timesb,
	timesbar: timesbar,
	timesd: timesd,
	tint: tint,
	toea: toea,
	top: top,
	topbot: topbot,
	topcir: topcir,
	topf: topf,
	topfork: topfork,
	tosa: tosa,
	tprime: tprime,
	triangle: triangle,
	triangledown: triangledown,
	triangleleft: triangleleft,
	trianglelefteq: trianglelefteq,
	triangleq: triangleq,
	triangleright: triangleright,
	trianglerighteq: trianglerighteq,
	tridot: tridot,
	trie: trie,
	triminus: triminus,
	triplus: triplus,
	trisb: trisb,
	tritime: tritime,
	trpezium: trpezium,
	tscr: tscr,
	tscy: tscy,
	tshcy: tshcy,
	tstrok: tstrok,
	twixt: twixt,
	twoheadleftarrow: twoheadleftarrow,
	twoheadrightarrow: twoheadrightarrow,
	uHar: uHar,
	ubrcy: ubrcy,
	ubreve: ubreve,
	ucy: ucy,
	udarr: udarr,
	udblac: udblac,
	udhar: udhar,
	ufisht: ufisht,
	ufr: ufr,
	uharl: uharl,
	uharr: uharr,
	uhblk: uhblk,
	ulcorn: ulcorn,
	ulcorner: ulcorner,
	ulcrop: ulcrop,
	ultri: ultri,
	umacr: umacr,
	uogon: uogon,
	uopf: uopf,
	uparrow: uparrow,
	updownarrow: updownarrow,
	upharpoonleft: upharpoonleft,
	upharpoonright: upharpoonright,
	uplus: uplus,
	upsi: upsi,
	upuparrows: upuparrows,
	urcorn: urcorn,
	urcorner: urcorner,
	urcrop: urcrop,
	uring: uring,
	urtri: urtri,
	uscr: uscr,
	utdot: utdot,
	utilde: utilde,
	utri: utri,
	utrif: utrif,
	uuarr: uuarr,
	uwangle: uwangle,
	vArr: vArr,
	vBar: vBar,
	vBarv: vBarv,
	vDash: vDash,
	vangrt: vangrt,
	varepsilon: varepsilon,
	varkappa: varkappa,
	varnothing: varnothing,
	varphi: varphi,
	varpi: varpi,
	varpropto: varpropto,
	varr: varr,
	varrho: varrho,
	varsigma: varsigma,
	varsubsetneq: varsubsetneq,
	varsubsetneqq: varsubsetneqq,
	varsupsetneq: varsupsetneq,
	varsupsetneqq: varsupsetneqq,
	vartheta: vartheta,
	vartriangleleft: vartriangleleft,
	vartriangleright: vartriangleright,
	vcy: vcy,
	vdash: vdash,
	vee: vee,
	veebar: veebar,
	veeeq: veeeq,
	vellip: vellip,
	vfr: vfr,
	vltri: vltri,
	vnsub: vnsub,
	vnsup: vnsup,
	vopf: vopf,
	vprop: vprop,
	vrtri: vrtri,
	vscr: vscr,
	vsubnE: vsubnE,
	vsubne: vsubne,
	vsupnE: vsupnE,
	vsupne: vsupne,
	vzigzag: vzigzag,
	wcirc: wcirc,
	wedbar: wedbar,
	wedge: wedge,
	wedgeq: wedgeq,
	wfr: wfr,
	wopf: wopf,
	wp: wp,
	wr: wr,
	wreath: wreath,
	wscr: wscr,
	xcap: xcap,
	xcirc: xcirc,
	xcup: xcup,
	xdtri: xdtri,
	xfr: xfr,
	xhArr: xhArr,
	xharr: xharr,
	xlArr: xlArr,
	xlarr: xlarr,
	xmap: xmap,
	xnis: xnis,
	xodot: xodot,
	xopf: xopf,
	xoplus: xoplus,
	xotime: xotime,
	xrArr: xrArr,
	xrarr: xrarr,
	xscr: xscr,
	xsqcup: xsqcup,
	xuplus: xuplus,
	xutri: xutri,
	xvee: xvee,
	xwedge: xwedge,
	yacy: yacy,
	ycirc: ycirc,
	ycy: ycy,
	yfr: yfr,
	yicy: yicy,
	yopf: yopf,
	yscr: yscr,
	yucy: yucy,
	zacute: zacute,
	zcaron: zcaron,
	zcy: zcy,
	zdot: zdot,
	zeetrf: zeetrf,
	zfr: zfr,
	zhcy: zhcy,
	zigrarr: zigrarr,
	zopf: zopf,
	zscr: zscr
};

var notEmailFriendlyMinLength = 2;
var notEmailFriendlyMaxLength = 31;

exports.notEmailFriendly = notEmailFriendly;
exports.notEmailFriendlyMaxLength = notEmailFriendlyMaxLength;
exports.notEmailFriendlyMinLength = notEmailFriendlyMinLength;
