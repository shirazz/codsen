# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.9.0 (2019-06-29)

### Features

- Add perf checking, recording and historic comparison ([173308e](https://gitlab.com/codsen/codsen/commit/173308e))

## 1.8.0 (2019-01-20)

- ✨ Various documentation and setup tweaks after we migrated to monorepo
- ✨ Setup refresh: updated dependencies and all config files using automated tools

## 1.4.0 (2018-12-14)

- ✨ Updated all dependencies and restored AVA linting, added licence to the top of each built file

## 1.3.0 (2018-10-12)

- ✨ Updated all dependencies and restored coverage reporting both in terminal and sending to coveralls

## 1.2.0 (2018-05-11)

- ✨ Pointed AVA unit tests to ES Modules build, as opposed to previously transpiled CommonJS-one. This means, now unit test code coverage is correct.

## 1.1.0 (2018-04-29)

- ✨ Setup refresh and dependency updates: deleted `package-lock.json` and `.editorconfig`

## 1.0.0 (2018-03-31)

- ✨ Public release
