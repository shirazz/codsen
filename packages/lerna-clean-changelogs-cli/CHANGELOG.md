# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.2.0 (2019-03-04)

### Features

- Enable blank cli calls without arguments ([ebc4755](https://gitlab.com/codsen/codsen/commit/ebc4755))

## 1.1.0 (2019-01-31)

### Features

- Update unit tests to match the API ([e53dc83](https://gitlab.com/codsen/codsen/commit/e53dc83))

## 1.0.0 (2019-01-20)

- ✨ First public release
