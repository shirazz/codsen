# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.2.2 (2020-02-24)

### Bug Fixes

- loop through the correct parts ([2d4c3f7](https://gitlab.com/codsen/codsen/commit/2d4c3f750032e694b7cff56839eab522989023d5))

## 1.2.0 (2020-01-26)

### Features

- add more brackets logic cases ([e83bc30](https://gitlab.com/codsen/codsen/commit/e83bc301da7c9a9cf406a13f7bd4993d9b268a4b))
- only and, only only, only not cases ([0df6970](https://gitlab.com/codsen/codsen/commit/0df697077ff938c59a5aac17faa05cd4f6bb93fe))
- semicolon safeguards ([f3cccc3](https://gitlab.com/codsen/codsen/commit/f3cccc3f23dce2aad8a8cb57d4836301e71efe69))
- tests create sample HTML's, improve logic in brackets ([9e96eb7](https://gitlab.com/codsen/codsen/commit/9e96eb7286df5a67012be3916c48c3043017938c))
- whitespace clauses ([5cb22cc](https://gitlab.com/codsen/codsen/commit/5cb22cc4c91d478568fd3b7919f6e2f5ea8eedfc))

## 1.1.0 (2020-01-12)

### Features

- write all the logic up until boolean joiners ([e8f918f](https://gitlab.com/codsen/codsen/commit/e8f918fa86eab81cb12277b2d86c5e9d5d7b6646))

## 1.0.0 (2020-01-05)

- ✨ First public release
