{
	"babelrc": {
		"cli": {
			"presets": [
				[
					"@babel/preset-env",
					{
						"modules": false,
						"targets": {
							"node": "4.0"
						}
					}
				]
			]
		},
		"set": {
			"env": {
				"test": {
					"plugins": [
						"istanbul"
					]
				}
			},
			"presets": [
				[
					"@babel/preset-env",
					{
						"modules": false
					}
				]
			]
		}
	},
	"badges": [
		{
			"gitlab": {
				"alt": "Repository is on GitLab",
				"img": "https://img.shields.io/badge/repo-on%20GitLab-brightgreen.svg?style=flat-square",
				"url": "https://gitlab.com/codsen/codsen/tree/master/packages/%REPONAME%"
			}
		},
		{
			"cov": {
				"alt": "Coverage",
				"img": "https://img.shields.io/badge/coverage-%COVPERC%-%COVBADGECOLOR%.svg?style=flat-square",
				"url": "https://gitlab.com/codsen/codsen/tree/master/packages/%REPONAME%"
			}
		},
		{
			"deps2d": {
				"alt": "View dependencies as 2D chart",
				"img": "https://img.shields.io/badge/deps%20in%202D-see_here-08f0fd.svg?style=flat-square",
				"url": "http://npm.anvaka.com/#/view/2d/%REPONAME%"
			}
		},
		{
			"downloads": {
				"alt": "Downloads/Month",
				"img": "https://img.shields.io/npm/dm/%REPONAME%.svg?style=flat-square",
				"url": "https://npmcharts.com/compare/%REPONAME%"
			}
		},
		{
			"runkit": {
				"alt": "Test in browser",
				"img": "https://img.shields.io/badge/runkit-test_in_browser-a853ff.svg?style=flat-square",
				"url": "https://npm.runkit.com/%REPONAME%"
			}
		},
		{
			"prettier": {
				"alt": "Code style: prettier",
				"img": "https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square",
				"url": "https://prettier.io"
			}
		},
		{
			"license": {
				"alt": "MIT License",
				"img": "https://img.shields.io/badge/licence-MIT-51c838.svg?style=flat-square",
				"url": "https://gitlab.com/codsen/codsen/blob/master/LICENSE"
			}
		}
	],
	"contributing": {
		"header": "## Contributing",
		"restofit": "- If you see an error, [raise an issue](<%ISSUELINK%>).\n- If you want a new feature but can't code it up yourself, also [raise an issue](<%ISSUELINK%>). Let's discuss it.\n- If you tried to use this package, but something didn't work out, also [raise an issue](<%ISSUELINK%>). We'll try to help.\n- If you want to contribute some code, fork the [monorepo](https://gitlab.com/codsen/codsen/) via GitLab, then write code, then file a pull request on GitLab. We'll merge it in and release.\n\nIn monorepo, npm libraries are located in `packages/` folder. Inside, the source code is located either in `src/` folder (normal npm library) or in the root, `cli.js` (if it's a command-line application).\n\nThe npm script \"`dev`\", the `\"dev\": \"rollup -c --dev\"` builds the development version retaining all `console.log`s with row numbers. It's handy to have [js-row-num-cli](https://www.npmjs.com/package/js-row-num-cli) installed globally so you can automatically update the row numbers on all `console.log`s."
	},
	"eslintrc": {
		"env": {
			"es6": true,
			"node": true
		},
		"extends": [
			"eslint:recommended",
			"plugin:prettier/recommended",
			"plugin:import/errors",
			"plugin:import/warnings"
		],
		"parserOptions": {
			"ecmaVersion": 2018,
			"sourceType": "module"
		},
		"plugins": [],
		"root": true,
		"rules": {
			"curly": "error",
			"no-constant-condition": [
				"error",
				{
					"checkLoops": false
				}
			],
			"no-else-return": "error",
			"no-inner-declarations": "error",
			"no-unneeded-ternary": "error",
			"no-useless-return": "error",
			"no-var": "error",
			"one-var": [
				"error",
				"never"
			],
			"prefer-arrow-callback": "error",
			"prefer-const": "error",
			"prefer-template": "error",
			"strict": "error",
			"symbol-description": "error",
			"yoda": [
				"error",
				"never",
				{
					"exceptRange": true
				}
			]
		}
	},
	"files": {
		"delete": [
			".gitignore",
			".editorconfig",
			".gitattributes",
			".bithoundrc",
			".eslintrc.json",
			".travis.yml",
			"bitbucket-pipelines.yml",
			"package-lock.json",
			"yarn.lock",
			".DS_Store",
			"util.esm.js",
			"testStats.json"
		],
		"write_hard": [
			{
				"contents": "",
				"name": ""
			},
			{
				"contents": "dist/\ntest/fixtures\nfixtures/\n*.json\ntestStats.md\n",
				"name": ".prettierignore"
			},
			{
				"contents": "package-lock=false\n",
				"name": ".npmrc"
			},
			{
				"contents": "MIT License\n\nCopyright (c) 2015-%YEAR% Roy Revelt and other contributors\n\nPermission is hereby granted, free of charge, to any person obtaining\na copy of this software and associated documentation files (the\n\"Software\"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be\nincluded in all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,\nEXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND\nNONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE\nLIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION\nOF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION\nWITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.",
				"name": "LICENSE"
			}
		],
		"write_soft": [
			{
				"contents": "",
				"name": ""
			}
		]
	},
	"footer": {
		"alwaysAddTheseLinks": {}
	},
	"header": {
		"dontQuoteDescription": true,
		"rightFloatedBadge": []
	},
	"licence": {
		"extras": [],
		"header": "## Licence",
		"restofit": "MIT License\n\nCopyright (c) 2015-%YEAR% Roy Revelt and other contributors"
	},
	"npmignore": {
		"badFiles": [
			".babelrc",
			".editorconfig",
			".eslintrc",
			".eslintrc.json",
			".gitattributes",
			".gitignore",
			".npmignore",
			".prettierignore",
			".travis.yml",
			"benchmark.js",
			"bitbucket-pipelines.yml",
			"chlu_adds_missing_diff_links.gif",
			"cli_bak.js",
			"contributing.md",
			"feature1.gif",
			"ideas.md",
			"package.json.lerna_backup",
			"rollup.config.js",
			"test.js",
			"test.zip",
			"test_bak.js",
			"testStats.json",
			"testStats.md",
			"travis.js"
		],
		"badFolders": [
			".idea",
			".nyc_output",
			"coverage",
			"docs",
			"fixtures",
			"media",
			"perf",
			"reference",
			"src",
			"tap",
			"temp",
			"test",
			"test_alt",
			"test-folder",
			"test-util",
			"t-util"
		],
		"goodFiles": [
			"cli-es5.js",
			"cli.js",
			"index-es5.js",
			"index.js",
			"init-npmignore.js",
			"runkit_example.js",
			"util.js"
		],
		"goodFolders": [
			"bin",
			"dist",
			"lib",
			"rules"
		]
	},
	"package": {
		"devDependencies": {
			"@babel/core": "^7.9.0",
			"@babel/preset-env": "^7.9.0",
			"@rollup/plugin-commonjs": "^11.0.2",
			"@rollup/plugin-node-resolve": "^7.1.1",
			"@rollup/plugin-strip": "^1.3.2",
			"tap": "^14.10.7",
			"benchmark": "^2.1.4",
			"lect": "^0.12.4",
			"rollup": "^2.3.2",
			"rollup-plugin-ascii": "^0.0.3",
			"rollup-plugin-babel": "^4.4.0",
			"rollup-plugin-cleanup": "^3.1.1",
			"rollup-plugin-banner": "^0.2.1",
			"rollup-plugin-terser": "^5.3.0"
		},
		"engines": {
			"node": ">=10"
		},
		"tap": {
			"coverage-report": [
				"text",
				"json-summary"
			],
			"timeout": 0
		},
		"husky": {
			"hooks": {
				"pre-commit": "npm run format && npm test"
			}
		},
		"license": "MIT"
	},
	"package_keys": {
		"write_hard": {
			"repository": "https://gitlab.com/codsen/codsen/"
		},
		"write_soft": {},
		"delete": [
			"",
			""
		]
	},
	"rollup": {
		"infoTable": {
			"cjsTitle": "Main export - **CommonJS version**, transpiled to ES5, contains `require` and `module.exports`",
			"esmTitle": "**ES module** build that Webpack/Rollup understands. Untranspiled ES6 code with `import`/`export`.",
			"umdTitle": "**UMD build** for browsers, transpiled, minified, containing `iife`'s and has all dependencies baked-in"
		},
		"infoTitle": "Here's what you'll get:"
	},
	"scripts": {
		"cli": {
			"dev": "echo\"\"",
			"devunittest": "npm run dev && ./node_modules/.bin/tap --only",
			"format": "npm run lect && npm run prettier && npm run lint",
			"lect": "lect",
			"lint": "../../node_modules/eslint/bin/eslint.js \"**/*.js\" --fix --config \"../../.eslintrc.json\"",
			"prettier": "../../node_modules/prettier/bin-prettier.js '*.{js,css,scss,vue,md}' --write",
			"tap": "tap",
			"test": "npm run lint && npm run unittest && npm run format",
			"unittest": "./node_modules/.bin/tap --no-only -R 'terse' -o testStats.md"
		},
		"rollup": {
			"build": "rollup -c",
			"dev": "rollup -c --dev",
			"devunittest": "npm run dev && ./node_modules/.bin/tap --only",
			"format": "npm run lect && npm run prettier && npm run lint",
			"lect": "lect",
			"lint": "../../node_modules/eslint/bin/eslint.js \"**/*.js\" --fix --config \"../../.eslintrc.json\"",
			"perf": "node perf/check",
			"prepare": "npm run build",
			"pretest": "npm run build",
			"prettier": "../../node_modules/prettier/bin-prettier.js '*.{js,css,scss,vue,md}' --write",
			"tap": "tap",
			"test": "npm run lint && npm run unittest && npm run format",
			"unittest": "./node_modules/.bin/tap --no-only -R 'terse' -o testStats.md && npm run perf",
			"version": "npm run build && git add ."
		}
	},
	"travis": false,
	"various": {
		"addBlanks": true,
		"back_to_top": {
			"enabled": true,
			"label": "⬆ back to top",
			"url": "#"
		}
	}
}
