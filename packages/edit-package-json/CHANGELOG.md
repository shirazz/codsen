# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 0.1.5 (2019-10-23)

### Bug Fixes

- fix the missing tests and enable all synthetic tests on all package.json's in monorepo ([b08c76a](https://gitlab.com/codsen/codsen/commit/b08c76a51546f9e02450e9bbd8eb3e52be1ecfe4))

## 0.1.4 (2019-10-21)

### Bug Fixes

- algorithm improvements, many cases fixed ([30a6cf8](https://gitlab.com/codsen/codsen/commit/30a6cf8ff639059507935ff348d4d9700ca8fec7))

## 0.1.3 (2019-10-09)

### Bug Fixes

- few bug fixes involving arrays, comments and quoted chunks ([337c7ca](https://gitlab.com/codsen/codsen/commit/337c7cad0935d7b1b2449d6b69175f5a3c02b8c4))

## 0.1.2 (2019-10-05)

### Bug Fixes

- fix array element deletion ([49e2124](https://gitlab.com/codsen/codsen/commit/49e2124))
- fix certain nested combinations + set up real-file based fixtures on a separate test file ([456a7f6](https://gitlab.com/codsen/codsen/commit/456a7f6))

## 0.1.0 (2019-09-23)

### Features

- complete the set() method, can't write new paths yet, only amend existing-ones ([2c6a430](https://gitlab.com/codsen/codsen/commit/2c6a430))
- del method to delete existing paths in JSON ([e29eae1](https://gitlab.com/codsen/codsen/commit/e29eae1))
- escaping of JSON-illegal strings like quotes ([dde3c5e](https://gitlab.com/codsen/codsen/commit/dde3c5e))
- make nested array/plain object state recognition reliable and precise ([9d51916](https://gitlab.com/codsen/codsen/commit/9d51916))

## 1.0.0 (2019-09-21)

- ✨ First public release
