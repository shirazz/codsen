# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.2.0 (2019-10-02)

### Features

- don't convert single apostrophe or double apostrophe if there's nothing around it ([5d940b3](https://gitlab.com/codsen/codsen/commit/5d940b3))
- explicit settings which decode if options are off ([4f1117a](https://gitlab.com/codsen/codsen/commit/4f1117a))

## 1.1.0 (2019-09-11)

### Features

- initial release ([04861c2](https://gitlab.com/codsen/codsen/commit/04861c2))

## 1.0.0 (2019-09-06)

- ✨ First public release
