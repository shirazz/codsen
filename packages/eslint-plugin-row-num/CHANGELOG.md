# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.1.1 (2019-12-14)

### Bug Fixes

- set up Tap as a test runner, write many more unit tests and fix all remaining issues ([58e1471](https://gitlab.com/codsen/codsen/commit/58e147195282077df7ad20efb00dac95976ac24d))

## 1.1.0 (2019-12-09)

### Features

- initial release ([6be989e](https://gitlab.com/codsen/codsen/commit/6be989ee0df3f06661a2319dc990c39d1c3e682f))
